(for Czech see below)

## Abstract and keywords
Master thesis deals with sound localisation accuracy in VR systems based on different HRTFs. Firstly, the thesis summarises key concepts of spatial hearing, HRTF measurement principles and VR system application design (with respect to spatial audio). Then 2 experimental workstations arosed on the Department of Radioelectronics, FEE CTU are presented to reader. The emphasis is mainly on their inconnection in order to HRTF measurement for VAS. Lastly the thesis deals with subjective test design in order to demonstrate the impact of HRTF on spatial hearing in VAS. The results of this tests are briefly discussed and possible conclusions for VR systems research are outlined.

**KW:** sound, HRTF, VR systems, VAS, Unity

# Vliv HRTF na polohu zdroje zvuku ve VR

Projekt obsahuje implementace pracovišť na katedře radioelektroniky FEL ČVUT v Praze pro lokalizaci zvuku ve VR vzniklá či upravená v rámci diplomové práce Martina Novotného *Vliv HRTF na polohu zdroje zvuku ve VR* (2022) a všechna dostupná data naměřená v rámci této diplomové práce.

## Abstrakt a klíčová slova

Diplomová práce se zabývá kvalitou prostorové lokalizace zdrojů zvuku ve VR systémech v závislosti na různých HRTF. Čtenář je v práci nejprve stručně seznámen se základní podstatou směrového slyšení, měření HRTF a využití těchto poznatků při tvorbě VR obsahu. Následně jsou v práci popsána 2 experimentální pracoviště vzniklá na katedře radioelektroniky FEL ČVUT v Praze a jejich vzájemné propojení za účelem sledování vlivu HRTF na polohu zdroje zvuku ve virtuálním prostředí. V poslední části práce je pak nastíněn návrh a průběh subjektivního měření HRTF a prostorové lokalizace zvukových zdrojů a jsou diskutovány jeho výsledky a důsledky pro další práci ve VR systémech.

**KW:** zvuk, HRTF, VR systémy, VAS, Unity
