%% Script for plotting data from SOFA file
% Author: Martin Novotny (novotm80@fel.cvut.cz)
close all; clear; clc;

% set LATEX as default interpreter (for visulalisations)
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% add folders to path
addpath(genpath('SOFA_API_MO'));
addpath(genpath('data'));

set(0,'defaultAxesFontSize',20, 'defaultAxesLineWidth', 2); % set default font size

%% Get data for read
SOFAstart()
tit = {'Left ear', 'Right ear'};

% select all SOFA files to plot (in GUI)
[file,path] = uigetfile('*.sofa', 'Select SOFA files', 'MultiSelect', 'on');

if isequal(file,0)
   errordlg('Nothing was selected');
else
    for idx = 1:length(file)
        sofa_obj = SOFAload([path file{idx}]); % load SOFA file and save it to array

        figure('units','normalized','outerposition',[0 0 1 1]);
        name = extractBefore(file{idx}, '.sofa');
        
        for rn = 1:2 % rn - receiver number (1 - left, 2 - right)
            subplot(1,2,rn)
            [M, metadata, h] = SOFAplotHRTF(sofa_obj, 'MagHorizontal', rn);

            h.XData = h.XData/1e3; % frequency Hz to kHz
            
            % axis limit
            h.Parent.XLim=[0.02 20];
            h.Parent.YLim=[-150 180];
            
            % axis legend
            h.Parent.XLabel.String = 'Frequency (kHz)';
            h.Parent.YLabel.String = 'Azimuth ($^\circ$)';
            
            title(tit{rn});    
            grid on;
            grid minor;
        end
        
        % plot of the whole figure
        tit_plot = sprintf('%s - measured data', name);
        sgtitle(tit_plot, 'FontSize', 25);

        % save figure to IMG folder
        cd img/color
            print(name, '-djpeg');
        cd ../../
        
    end
end