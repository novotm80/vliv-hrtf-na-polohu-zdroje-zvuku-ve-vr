%% Script for plotting data from SOFA file
% Author: Martin Novotny (novotm80@fel.cvut.cz)
close all; clear; clc;

% set LATEX as default interpreter (for visulalisations)
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% add folders to path
addpath(genpath('SOFA_API_MO'));
addpath(genpath('data'));

set(0,'defaultAxesFontSize',20, 'defaultAxesLineWidth', 2); % set default font size

%% Get data for read
SOFAstart() % start SOFA API
tit = {'Left ear', 'Right ear'};
az_sel = 0; % select azimuth for plotting

% select all SOFA files to plot (in GUI)
[file,path] = uigetfile('*.sofa', 'Select SOFA files', 'MultiSelect', 'on');

if isequal(file,0)
   errordlg('Nothing was selected');
else

    for idx = 1:length(file)
        sofa_obj = SOFAload([path file{idx}]); % load SOFA file and save it to array
        
        figure('units','normalized','outerposition',[0 0 1 1]);
        name = extractBefore(file{idx},'.sofa');
        
        for rn = 1:2 % rn - receiver number (1 - left, 2 - right)
            subplot(2,1,rn)
            [M, metadata, h] = SOFAplotHRTF(sofa_obj, 'MagSpectrum', rn, 'dir', az_sel); % plot frequency spectrum of SOFA file
            
            % set axis type
            h(1).Parent.XScale='log';
            h(1).Parent.Parent.Units = 'normalized';
            h(1).Parent.Parent.OuterPosition = [0 0 1 1];
            h(1).Parent.Legend.Location = 'northeastoutside';
            h(1).Parent.XLim = [200 20e3];
     
            grid on;
            grid minor;
            title(tit{end});
        end
        
        % title of the whole figure
        tit_plot = sprintf('%s - measured data', name);
        sgtitle(tit_plot, 'FontSize', 25);
        
        % save figure to IMG folder
        cd img/spectrum
            print(name, '-djpeg');
        cd ../../
    end
end
