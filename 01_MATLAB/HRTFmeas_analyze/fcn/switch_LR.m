input_path='namerena_data\supka_001_AuSim_raw_data_SweepSine.mat';

load(input_path);

names_ele=fieldnames(raw_data);
for i_ele=1:length(names_ele)
    if strncmpi('e',names_ele{i_ele},1)==1 % p�esko�en� metadat
       eval(strcat('names_az=fieldnames(raw_data.',names_ele{i_ele},');'));
       for i_az=1:length(names_az)
           eval(strcat('raw_data1.',names_ele{i_ele},'.',names_az{i_az},'.left=raw_data.',names_ele{i_ele},'.',names_az{i_az},'.right;')); %do processed_data se nacpou o��znut� nefiltrovan� IRky
           eval(strcat('raw_data1.',names_ele{i_ele},'.',names_az{i_az},'.right=raw_data.',names_ele{i_ele},'.',names_az{i_az},'.left;'));
       end
    end
end
raw_data1.metadata=raw_data.metadata;

clear raw_data

raw_data=raw_data1;

save(input_path,'raw_data');

