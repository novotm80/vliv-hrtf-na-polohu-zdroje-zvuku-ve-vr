clear all; clc; %close all;

tauIR=1; % doba dozvuku z�kl. IR [s]
tauIR2=0.01; % doba dozvuku 2. IR [s]
kmax=5; % max po�et harmonick�ch
f1=100; f2=20000; %hrani�n� frekvence sweepu [Hz]
tau_SW=[0.5:0.5:5]; % doba trv�n� sweepu [s]
% tau_SW=0.3;
N=6; % po�et reproduktor�
n=2; % po�et syst�mu s IL 

%% ES
tau_ES=N*(tau_SW+tauIR); % doba trv�n� m��en� bez OL a IL
r_s=(log2(f2/f1))./tau_SW; % sweep rate
delta_t_kmax=log2(kmax)./r_s; % vzd�lenost vlastn� IR od posledn� harmonick� [s]
delta_t_2=log2(2)./r_s; % vzd�lenost vlastn� IR od druh� harmonick� [s]

if r_s>(1/tauIR2) % podm�nka pro sweep
    errordlg('Vlastn� IR koliduje s 2. harmonickou. Sni�te rychlost p�ela�ov�n� sweepu!','Sweep error');
end

%% OL
delta_T_OL=delta_t_kmax+tauIR; % minim�ln� zpo�d�n� mezi sweepy
tauOL=tau_SW+(N-1)*(delta_t_kmax+tauIR); % doba trv�n� m��en� p�i pou�it� OL 

disp('OL'); disp(' ');
disp(['Minim�ln� zpo�d�n� mezi sweepy: ' num2str(delta_T_OL) 's']);

%%
figure(1)
% plot(tau_SW,tau_ES,'LineWidth',2); 
hold on;
plot(tau_SW,tauOL,'LineWidth',2); %hold off;
% legend('bez IL a OL', 's OL');

legend('bez IL a OL', 's OL (k_{max}=2)', 's OL (k_{max}=3)', 's OL (k_{max}=4)', 's OL (k_{max}=5)');
xlabel('doba trvani jednotliveho sweepu [s]')
ylabel('doba trvani cel�ho mereni pro N systemu [s]')
title(horzcat('\tau_{IR} = ',num2str(tauIR),' s, \tau_{IR,2} = ',num2str(tauIR2),' s, N = ', num2str(N),', f_1 = 100 Hz, f_2 = 20 kHz'))

%%

