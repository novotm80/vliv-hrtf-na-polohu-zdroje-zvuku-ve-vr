function fftcko(h,fs)
spectrum=abs(fft(h));
spectrum=spectrum(1:ceil(end/2));
l=length(spectrum);
semilogx(1:(fs/2)/l:fs/2,spectrum); axis tight