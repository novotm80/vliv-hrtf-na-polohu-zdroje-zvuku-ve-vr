function ir = analyze_sine_sweep(mes_sig,inv_filter,repetitions)

if nargin==2
    repetitions=1;
end
ir=zeros(1,length(mes_sig)/repetitions+length(inv_filter)-1);
for i=1:repetitions
    ir=ir+conv(mes_sig((i-1)*(length(mes_sig)/repetitions)+1:i*(length(mes_sig)/repetitions))', inv_filter');
end
ir=ir/repetitions;
