function [sig_mat, sweep_out, inv_filter, delta_T_OL_sam]=generate_SS_OL_matrix(fs,tauIR,tauIR2,kmax,f1,f2,N_SW,N,ham)


% clc; close all ; clear all;
% fs=96000;
% 
% tauIR=0.7; % doba dozvuku z�kl. IR [s]
% tauIR2=0.03; % doba dozvuku 2. IR [s]
% kmax=3; % max po�et harmonick�ch
% f1=100; f2=20000; %hrani�n� frekvence sweepu [Hz]
% tau_SW=0.4; % doba trv�n� sweepu [s]
% N=6;
tau_SW=(2^N_SW)/fs;
%% ES
r_s=(log2(f2/f1))/tau_SW; % sweep rate
delta_t_kmax=log2(kmax)/r_s; % vzd�lenost vlastn� IR od posledn� harmonick� [s]

if r_s>(1/tauIR2) % podm�nka pro sweep
    errordlg('Vlastn� IR koliduje s 2. harmonickou. Sni�te rychlost p�ela�ov�n� sweepu!','Sweep error');
end

%% OL
delta_T_OL=delta_t_kmax+tauIR;
delta_T_OL_sam=ceil(delta_T_OL*fs); %ve vzork�ch

%% gen sweep

[sweep_out, inv_filter, t]=generate_sine_sweep(fs,tau_SW,f1,f2,ham,1);
sweep_out=sweep_out*0.9;
sig_mat=zeros(length(sweep_out)+(N-1)*delta_T_OL_sam,N);

start=1;
stop=length(sweep_out);
for i=1:N
    sig_mat(start:stop,i)=sweep_out;
    start=start+length(sweep_out)+delta_T_OL_sam;
    stop=stop+length(sweep_out)+delta_T_OL_sam;
end

% audiowrite('sweepOL_matrix.wav',sig_mat,fs);