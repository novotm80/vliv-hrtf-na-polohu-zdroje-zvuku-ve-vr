[sequence, mls] = GenerateMLSSequence(5, 18, 0);
spec=fft(mls);
subplot(211); plot(mag2db(abs(spec)),'LineWidth',2); xlabel('Frekvence [Hz]'); ylabel('Modul [dB]'); title('Modulov� spektrum');
subplot(212); plot(angle(spec),'.'); xlabel('Frekvence [Hz]'); ylabel('F�ze [rad]'); title('F�zov� spektrum');