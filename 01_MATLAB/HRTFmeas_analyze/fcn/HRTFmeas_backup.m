   function varargout = HRTFmeas(varargin)
% HRTFMEAS MATLAB code for HRTFmeas.fig
%      HRTFMEAS, by itself, creates a new HRTFMEAS or raises the existing
%      singleton*.
%
%      H = HRTFMEAS returns the handle to a new HRTFMEAS or the handle to
%      the existing singleton*.
%
%      HRTFMEAS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HRTFMEAS.M with the given input arguments.
%
%      HRTFMEAS('Property','Value',...) creates a new HRTFMEAS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HRTFmeas_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HRTFmeas_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HRTFmeas

% Last Modified by GUIDE v2.5 11-Jun-2015 10:29:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HRTFmeas_OpeningFcn, ...
                   'gui_OutputFcn',  @HRTFmeas_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HRTFmeas is made visible.
function HRTFmeas_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HRTFmeas (see VARARGIN)

% Choose default command line output for HRTFmeas
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global info;
info.connected=0;
info.pozice=0;

plot_dist(handles);
methods={'Sine Sweep (log)','MLS','MESM'};
set(handles.method_pop,'String',methods);



% UIWAIT makes HRTFmeas wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = HRTFmeas_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in turn_test.
function turn_test_Callback(hObject, eventdata, handles)
% hObject    handle to turn_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
if info.connected==1
    test_turn=str2num(get(handles.turn_test1,'String'));
    for i=1:length(test_turn)
        turn(test_turn(i),handles);
    end
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end




function turn_test1_Callback(hObject, eventdata, handles)
% hObject    handle to turn_test1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of turn_test1 as text
%        str2double(get(hObject,'String')) returns contents of turn_test1 as a double


% --- Executes during object creation, after setting all properties.
function turn_test1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to turn_test1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in back_to_zero.
function back_to_zero_Callback(hObject, eventdata, handles)
% hObject    handle to back_to_zero (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
turn(-info.pozice,handles);


% --- Executes on slider movement.
function speed_Callback(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
send_speed(handles);


function send_speed(handles)
global ard;
result = round(get(handles.speed,'Value')/0.2)*0.2;
set(handles.speed_text1,'Value',result);
set(handles.speed_text1,'String',num2str(result));
x=1;
while x<11
    if(result==0.2*x)
        fwrite(ard,250-x);
        x=11;
    end
    x=x+1;
end

% --- Executes during object creation, after setting all properties.
function speed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function ard_port_Callback(hObject, eventdata, handles)
% hObject    handle to ard_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ard_port as text
%        str2double(get(hObject,'String')) returns contents of ard_port as a double


% --- Executes during object creation, after setting all properties.
function ard_port_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ard_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ard_connect.
function ard_connect_Callback(hObject, eventdata, handles)
% hObject    handle to ard_connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
global ard;
if info.connected==0
    delete(instrfind({'Port'},{get(handles.ard_port, 'String')})) % pro jistotu - p�i neodpojen� z�stane viset v pam�ti
    ard=serial(get(handles.ard_port, 'String'),'BaudRate',115200);  %zaveden� Arduina jako s�riov� objekt
    if exist('ard','var') && isvalid(ard),
        fopen(ard); %otev�en� pro z�pis
        pause(2);
        fwrite(ard,255); %pos�l�n� ��sla 255 = inicializace
        info.connected=1;
        set(handles.ard_status, 'String','p�ipojeno')
        set(handles.ard_status, 'ForegroundColor','green')
        set(handles.text_info,'String','Arduino bylo �spe�n� p�ipojeno.')
        set(handles.ard_connect, 'String', 'Odpojit');
    else
        set(handles.text_info,'String','Spojen� nebylo nav�z�no.')
    end
else
    delete(instrfind({'Port'},{get(handles.ard_port, 'String')}))
    set(handles.ard_status, 'String','odpojeno')
    set(handles.ard_status, 'ForegroundColor','red')
    set(handles.text_info,'String','Arduino bylo �spe�n� odpojeno.')
    set(handles.ard_connect, 'String', 'P�ipojit');
    pause(0.5);
    info.connected=0;
end


% --- Executes on button press in testrun.
function testrun_Callback(hObject, eventdata, handles)
% hObject    handle to testrun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of testrun

function turn(angle, handles)
send_speed(handles);
global ard;
global info;
if get(handles.testrun,'Value')==0 %pro testovac� motor je jin� ��zen�
    steps=angle*30;
else
    steps=round((angle/1.8)*8); %testovac� motor
end
info.pozice=info.pozice+angle; %evidence pozice
if steps<0 %zm�na sm�ru ot��en�
    steps=abs(steps);
    fwrite(ard,250);
else
    fwrite(ard,251);
end
pause(0.2);
if(get(handles.fade_run,'Value')==0) %pokud nen� zvolen postupn� rozb�h/dob�h
    while(steps>0) %cyklus ot��en� - d�len� celkov�ho po�tu instrukc� po max. 230
       if(steps>230)
           fwrite(ard,230);
           steps=steps-230;
       else
           fwrite(ard,steps);           
           steps=0;
       end       
    end
    doba=(((11-(get(handles.speed,'Value')/0.2))*200)*2*abs(angle)*30)/1000000;
    pause(doba);
else
    
%     fwrite(ard,252); steps=step-20; %rozb�h
%     while(steps>20)
%        if(steps>230)
%            fwrite(ard,230);
%            steps=steps-230;
%        else
%            fwrite(ard,steps);
%            steps=0;
%        end       
%     end
%     fwrite(ard,253); steps=0; %dob�h
end


% --- Executes on button press in fade_run.
function fade_run_Callback(hObject, eventdata, handles)
% hObject    handle to fade_run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fade_run


% --- Executes on button press in layout.
function layout_Callback(hObject, eventdata, handles)
% hObject    handle to layout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in method_pop.
function method_pop_Callback(hObject, eventdata, handles)
% hObject    handle to method_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method_pop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method_pop
metoda=get(handles.method_pop,'String');
popupvalue = metoda{get(handles.method_pop,'Value')};
switch(popupvalue)
    case 'Sine Sweep (log)'
        set(handles.text19,'String','D�lka [s] =');
        set(handles.seq_length,'String',num2str(2));
    case 'MLS'
        set(handles.text19,'String','D�lka sekvence 2^(N-1), N =');
        set(handles.seq_length,'String',num2str(16));
    case 'MESM'
        
end


% --- Executes during object creation, after setting all properties.
function method_pop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on selection change in dist_pop.
function dist_pop_Callback(hObject, eventdata, handles)
% hObject    handle to dist_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dist_pop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dist_pop


% --- Executes during object creation, after setting all properties.
function dist_pop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dist_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation1_Callback(hObject, eventdata, handles)
% hObject    handle to elevation1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation1 as text
%        str2double(get(hObject,'String')) returns contents of elevation1 as a double


% --- Executes during object creation, after setting all properties.
function elevation1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth1_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth1 as text
%        str2double(get(hObject,'String')) returns contents of azimuth1 as a double


% --- Executes during object creation, after setting all properties.
function azimuth1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation2_Callback(hObject, eventdata, handles)
% hObject    handle to elevation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation2 as text
%        str2double(get(hObject,'String')) returns contents of elevation2 as a double


% --- Executes during object creation, after setting all properties.
function elevation2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth2_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth2 as text
%        str2double(get(hObject,'String')) returns contents of azimuth2 as a double


% --- Executes during object creation, after setting all properties.
function azimuth2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation3_Callback(hObject, eventdata, handles)
% hObject    handle to elevation3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation3 as text
%        str2double(get(hObject,'String')) returns contents of elevation3 as a double


% --- Executes during object creation, after setting all properties.
function elevation3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth3_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth3 as text
%        str2double(get(hObject,'String')) returns contents of azimuth3 as a double


% --- Executes during object creation, after setting all properties.
function azimuth3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation4_Callback(hObject, eventdata, handles)
% hObject    handle to elevation4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation4 as text
%        str2double(get(hObject,'String')) returns contents of elevation4 as a double


% --- Executes during object creation, after setting all properties.
function elevation4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth4_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth4 as text
%        str2double(get(hObject,'String')) returns contents of azimuth4 as a double


% --- Executes during object creation, after setting all properties.
function azimuth4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check1.
function check1_Callback(hObject, eventdata, handles)
% hObject    handle to check1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check1



function elevation5_Callback(hObject, eventdata, handles)
% hObject    handle to elevation5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation5 as text
%        str2double(get(hObject,'String')) returns contents of elevation5 as a double


% --- Executes during object creation, after setting all properties.
function elevation5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth5_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth5 as text
%        str2double(get(hObject,'String')) returns contents of azimuth5 as a double


% --- Executes during object creation, after setting all properties.
function azimuth5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation6_Callback(hObject, eventdata, handles)
% hObject    handle to elevation6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation6 as text
%        str2double(get(hObject,'String')) returns contents of elevation6 as a double


% --- Executes during object creation, after setting all properties.
function elevation6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth6_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth6 as text
%        str2double(get(hObject,'String')) returns contents of azimuth6 as a double


% --- Executes during object creation, after setting all properties.
function azimuth6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check2.
function check2_Callback(hObject, eventdata, handles)
% hObject    handle to check2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check2


% --- Executes on button press in check3.
function check3_Callback(hObject, eventdata, handles)
% hObject    handle to check3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check3


% --- Executes on button press in check4.
function check4_Callback(hObject, eventdata, handles)
% hObject    handle to check4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check4


% --- Executes on button press in check5.
function check5_Callback(hObject, eventdata, handles)
% hObject    handle to check5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check5


% --- Executes on button press in check6.
function check6_Callback(hObject, eventdata, handles)
% hObject    handle to check6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check6

function plot_dist(handles)
axes(handles.layout1);
polar(0,0);hold on;view([85 20])
rem_lab = findall(gca,'type','text');
legit = {'0','30','60','90','120','150','180','210','240','270','300','330','360',''};
idx = ~ismember(get(rem_lab,'string'),legit);
set(rem_lab(idx),'string','')
azimut=[];elevace=[];
if(get(handles.check1,'Value')==1)
azimut=str2num(get(handles.azimuth1,'String'));
i=length(azimut);
elevace=90-ones(1,i)*str2double(get(handles.elevation1,'String'));
end
if(get(handles.check2,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth2,'String'))];
i=length(str2num(get(handles.azimuth2,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation2,'String'))];
end
if(get(handles.check3,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth3,'String'))];
i=length(str2num(get(handles.azimuth3,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation3,'String'))];
end
if(get(handles.check4,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth4,'String'))];
i=length(str2num(get(handles.azimuth4,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation4,'String'))];
end
if(get(handles.check5,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth5,'String'))];
i=length(str2num(get(handles.azimuth5,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation5,'String'))];
end
if(get(handles.check6,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth6,'String'))];
i=length(str2num(get(handles.azimuth6,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation6,'String'))];
end
x=sin(deg2rad(elevace)).*cos(deg2rad(azimut));
y=sin(deg2rad(elevace)).*sin(deg2rad(azimut));
for i=1:length(elevace)
    if elevace(i)<0
        z(i)=-cos(deg2rad(elevace(i)));
    else
        z(i)=cos(deg2rad(elevace(i)));
    end

end
h1=scatter3(x,y,z,25,'filled'); hold on; 
scatter3(0,0,0,500,'filled')
hold off

% --- Executes on button press in save_dist.
function save_dist_Callback(hObject, eventdata, handles)
% hObject    handle to save_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(get(handles.check1,'Value')==1)
    len=length(str2num(get(handles.azimuth1,'String')));
    dist_matrix(1,:)=[str2num(get(handles.elevation1,'String')) str2num(get(handles.azimuth1,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(1,:)=[555 zeros(1,360-1)];
end
if(get(handles.check2,'Value')==1)    
    len=length(str2num(get(handles.azimuth2,'String')));
    dist_matrix(2,:)=[str2num(get(handles.elevation2,'String')) str2num(get(handles.azimuth2,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(2,:)=[555 zeros(1,360-1)];
end
if(get(handles.check3,'Value')==1) 
    len=length(str2num(get(handles.azimuth3,'String')));
    dist_matrix(3,:)=[str2num(get(handles.elevation3,'String')) str2num(get(handles.azimuth3,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(3,:)=[555 zeros(1,360-1)];
end
if(get(handles.check4,'Value')==1) 
    len=length(str2num(get(handles.azimuth4,'String')));
    dist_matrix(4,:)=[str2num(get(handles.elevation4,'String')) str2num(get(handles.azimuth4,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(4,:)=[555 zeros(1,360-1)];
end
if(get(handles.check5,'Value')==1) 
    len=length(str2num(get(handles.azimuth5,'String')));
    dist_matrix(5,:)=[str2num(get(handles.elevation5,'String')) str2num(get(handles.azimuth5,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(5,:)=[555 zeros(1,360-1)];
end
if(get(handles.check6,'Value')==1) 
    len=length(str2num(get(handles.azimuth6,'String')));
    dist_matrix(6,:)=[str2num(get(handles.elevation6,'String')) str2num(get(handles.azimuth6,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(6,:)=[555 zeros(1,360-1)];
end
[nazev, path] = uiputfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
varname = genvarname(nazev1);
eval([varname '=dist_matrix;']);
save(full_path,nazev1);


% --- Executes on button press in plot_it.
function plot_it_Callback(hObject, eventdata, handles)
% hObject    handle to plot_it (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
plot_dist(handles)


% --- Executes on button press in load_dist.
function load_dist_Callback(hObject, eventdata, handles)
% hObject    handle to load_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[nazev, path] = uigetfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
load(full_path);
varname= genvarname(nazev1);
eval(['dist_matrix=' varname ';']);
clear azim
if (dist_matrix(1,1)~=555)
    set(handles.check1,'Value',1);
    set(handles.elevation1,'String',num2str(dist_matrix(1,1)));
    for i=2:360
        if dist_matrix(1,i)~=555
            azim(i-1)=dist_matrix(1,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth1,'String',no_add_spaces); 
end
clear azim
if (dist_matrix(2,1)~=555)
    set(handles.check2,'Value',1);
    set(handles.elevation2,'String',num2str(dist_matrix(2,1)));
    for i=2:360
        if dist_matrix(2,i)~=555
            azim(i-1)=dist_matrix(2,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth2,'String',no_add_spaces); 
end
clear azim
if (dist_matrix(3,1)~=555)
    set(handles.check3,'Value',1);
    set(handles.elevation3,'String',num2str(dist_matrix(3,1)));
    for i=2:360
        if dist_matrix(3,i)~=555
            azim(i-1)=dist_matrix(3,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth3,'String',no_add_spaces);  
end
clear azim
if (dist_matrix(4,1)~=555)
    set(handles.check4,'Value',1);
    set(handles.elevation4,'String',num2str(dist_matrix(4,1)));
    for i=2:360
        if dist_matrix(4,i)~=555
            azim(i-1)=dist_matrix(4,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth4,'String',no_add_spaces); 
end
clear azim
if (dist_matrix(5,1)~=555)
    set(handles.check5,'Value',1);
    set(handles.elevation5,'String',num2str(dist_matrix(5,1)));
    for i=2:360
        if dist_matrix(5,i)~=555
            azim(i-1)=dist_matrix(5,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth5,'String',no_add_spaces);  
end
clear azim
if (dist_matrix(6,1)~=555)
    set(handles.check6,'Value',1);
    set(handles.elevation6,'String',num2str(dist_matrix(6,1)));
    for i=2:360
        if dist_matrix(6,i)~=555
            azim(i-1)=dist_matrix(6,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth6,'String',no_add_spaces); 
end
plot_dist(handles)

% --- Executes during object creation, after setting all properties.
function sync_channel_in_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sync_channel_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function sync_channel_out_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sync_channel_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fs_Callback(hObject, eventdata, handles)
% hObject    handle to fs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fs as text
%        str2double(get(hObject,'String')) returns contents of fs as a double
refl=str2double(get(handles.reflection_dist,'String'));
fs=str2double(get(handles.fs,'String'));
vzorky=floor((1/340)*refl*fs);
set(handles.window_length,'String',num2str(vzorky));


% --- Executes during object creation, after setting all properties.
function fs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in input_channels.
function input_channels_Callback(hObject, eventdata, handles)
% hObject    handle to input_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function input_channels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function output_channels_Callback(hObject, eventdata, handles)
% hObject    handle to output_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_channels as text
%        str2double(get(hObject,'String')) returns contents of output_channels as a double


% --- Executes during object creation, after setting all properties.
function output_channels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
metoda=get(handles.method_pop,'String');
popupvalue = metoda{get(handles.method_pop,'Value')};
fs=str2double(get(handles.fs,'String'));
switch(popupvalue)
    case 'Sine Sweep (log)'
        sig=generate_sine_sweep(fs,str2double(get(handles.seq_length,'String')),20,22050,str2double(get(handles.seq_length,'String'))*0.05,str2double(get(handles.seq_count,'String')));
    case 'MLS'
        sig=GenerateMLSSequence(str2double(get(handles.seq_count,'String')),str2double(get(handles.seq_length,'String')),0);
    case 'MESM'
        warndlg('Metoda nen� implementov�na','Metoda nen� implementov�na');
end
meaSig  = sig';
sync = meaSig;
playDeviceName = 'default';
outputCh = str2double(strsplit(get(handles.output_channels,'String'),','))
recDeviceName = 'default';
inputCh = str2double(strsplit(get(handles.input_channels,'String'),','))
bufferSize = 2048;
no_azimut=0;
for i=1:6
    if eval(strcat('get(handles.check',num2str(i),',''Value'')==1'))
        no_azimut=no_azimut+1;
        if mod(no_azimut,2)==0 %vyber smeru rotace...aby se stridala
            if get(handles.CW,'Value')==1
                rotace=1;
            else
                rotace=0;
            end
        else
            if get(handles.CW,'Value')==1
                rotace=0;
            else
                rotace=1;
            end
        end
        eval(strcat('elevace=str2num(get(handles.elevation',num2str(i),',''String''));'));
        show_dlg_pozice(num2str(elevace));
        eval(strcat('azimuty=str2num(get(handles.azimuth',num2str(i),',''String''));'));
        azimuty=sort(azimuty); %serazeni azimutu, aby se slo postupne
        for ii=1:length(azimuty)
            set(handles.text_info,'String',horzcat('Aktualni merena elevace: ', num2str(elevace),'. Aktualni mereny azimut: ', num2str(azimuty(ii)),'.'));
            if ii==1
                pohyb=azimuty(ii);
            else
                pohyb=azimuty(ii)-azimuty(ii-1);
            end
            tic
            if rotace==1
                turn(pohyb,handles);
            else
                turn(-pohyb,handles);
            end
            toc
            recData = PaR(meaSig, sync ,playDeviceName,outputCh,recDeviceName,inputCh,bufferSize, fs);
            IRka(i,ii,:)=recData(:,1);
            figure(1)
            subplot(211); plot(recData(:,1));
            subplot(212); plot(recData(:,2));
        end
        
        pohyb=360-azimuty(ii);
        if rotace==1
             turn(pohyb,handles);
        else
             turn(-pohyb,handles);
        end
    end
end    
if info.pozice~=0
    turn(-info.pozice,handles);
end
set(handles.text_info,'String','Mereni dokonceno');
save('IRka.mat','IRka')


% figure(1)
% plot(sig)

function show_dlg_pozice(hodnota)
    d = dialog('Position',[300 300 250 150],'Name','Instrukce');

    txt = uicontrol('Parent',d,...
               'Style','text',...
               'Position',[20 80 210 60],...
               'String',horzcat('Nastavte reproduktor do �hlu ', hodnota, '�. Po celou dobu m��en� udr�ujte konstatn� vz�dlenost od hlavy. Po stisknut� OK za�ne m��ic� sekvence.'));

    btn = uicontrol('Parent',d,...
               'Position',[85 20 70 25],...
               'String','OK',...
               'Callback','delete(gcf)');
    uiwait(d);




% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in sound_test.
function sound_test_Callback(hObject, eventdata, handles)
% hObject    handle to sound_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sig=generate_sine_sweep(str2double(get(handles.fs,'String')),3,20,22050,3*0.05,1);
meaSig  = sig';
sync = meaSig;
playDeviceName = 'default';
outputCh = str2double(strsplit(get(handles.output_channels,'String'),','))
recDeviceName = 'default';
inputCh = str2double(strsplit(get(handles.input_channels,'String'),','))
bufferSize = 2048;

[recData] = PaR(meaSig,sync,playDeviceName,outputCh,recDeviceName,inputCh,bufferSize,str2double(get(handles.fs,'String')));
%testPaR();
figure;
subplot(211);
plot(recData(:,1));
subplot(212);
plot(recData(:,2));



function seq_length_Callback(hObject, eventdata, handles)
% hObject    handle to seq_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of seq_length as text
%        str2double(get(hObject,'String')) returns contents of seq_length as a double


% --- Executes during object creation, after setting all properties.
function seq_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seq_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function seq_count_Callback(hObject, eventdata, handles)
% hObject    handle to seq_count (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of seq_count as text
%        str2double(get(hObject,'String')) returns contents of seq_count as a double


% --- Executes during object creation, after setting all properties.
function seq_count_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seq_count (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in seq_pre.
function seq_pre_Callback(hObject, eventdata, handles)
% hObject    handle to seq_pre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of seq_pre


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in meas_ir.
function meas_ir_Callback(hObject, eventdata, handles)
% hObject    handle to meas_ir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in eq.
function eq_Callback(hObject, eventdata, handles)
% hObject    handle to eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of eq



function window_length_Callback(hObject, eventdata, handles)
% hObject    handle to window_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of window_length as text
%        str2double(get(hObject,'String')) returns contents of window_length as a double
vzorky=str2double(get(handles.window_length,'String'));
fs=str2double(get(handles.fs,'String'));
refl=round(vzorky*340/fs,2);
set(handles.reflection_dist,'String',num2str(refl));


% --- Executes during object creation, after setting all properties.
function window_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to window_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function reflection_dist_Callback(hObject, eventdata, handles)
% hObject    handle to reflection_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of reflection_dist as text
%        str2double(get(hObject,'String')) returns contents of reflection_dist as a double
refl=str2double(get(handles.reflection_dist,'String'));
fs=str2double(get(handles.fs,'String'));
vzorky=floor((1/340)*refl*fs);
set(handles.window_length,'String',num2str(vzorky));


% --- Executes during object creation, after setting all properties.
function reflection_dist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to reflection_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ir_cut.
function ir_cut_Callback(hObject, eventdata, handles)
% hObject    handle to ir_cut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ir_cut


% --- Executes on button press in motor_ena.
function motor_ena_Callback(hObject, eventdata, handles)
% hObject    handle to motor_ena (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of motor_ena
global ard;
if(get(handles.motor_ena,'Value')==1)
fwrite(ard,252);
else
fwrite(ard,253);    
end