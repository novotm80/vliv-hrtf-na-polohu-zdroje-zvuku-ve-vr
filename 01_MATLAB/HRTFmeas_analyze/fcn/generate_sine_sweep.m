function [sweep_out, inv_filter,t]= generate_sine_sweep(fs,Tp,f1,f2,fade,repetitions)

% fs=44100; %sample frcy
% Tp=1; %duration in seconds
% f1=20; %start frcy
% f2=20000; %stop frcy
% fade=0.01; %length of fade in and fade out in seconds (half of Hann window used)

if nargin==5
    repetitions=1;
end

t=0:1/fs:Tp-1/fs;
sweep = sin(((2*pi*f1*Tp)/log(f2/f1))*((f2/f1).^(t/Tp)-1));

if fade~=0
    maska=hann(2*fade*fs);
    maska1=maska(1:fade*fs);
    maska2=maska(fade*fs+1:end);
%     length(sweep)
%     length(maska1)
%     length(maska2)
%     length(t)
%     length([maska1;ones(Tp*fs-2*fade*fs,1);maska2])
    sweep=sweep.*[maska1;ones(Tp*fs-2*fade*fs,1);maska2]';
end

alfa=(0.3*log(10)*log2(f2/f1))./Tp;
inv_filter=fliplr(sweep)'.*exp(-alfa.*t)';

sweep_out=[];
for i=1:repetitions
    sweep_out=[sweep_out sweep];
end
t=t';
sweep_out=sweep_out'*0.95; %avoid clipping