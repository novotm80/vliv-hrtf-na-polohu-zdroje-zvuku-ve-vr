function varargout = PolarMeasure(varargin)
% POLARMEASURE MATLAB code for PolarMeasure.fig
%      POLARMEASURE, by itself, creates a new POLARMEASURE or raises the existing
%      singleton*.
%
%      H = POLARMEASURE returns the handle to a new POLARMEASURE or the handle to
%      the existing singleton*.
%
%      POLARMEASURE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in POLARMEASURE.M with the given input arguments.
%
%      POLARMEASURE('Property','Value',...) creates a new POLARMEASURE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PolarMeasure_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PolarMeasure_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PolarMeasure

% Last Modified by GUIDE v2.5 16-Apr-2014 11:42:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PolarMeasure_OpeningFcn, ...
                   'gui_OutputFcn',  @PolarMeasure_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PolarMeasure is made visible.
function PolarMeasure_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PolarMeasure (see VARARGIN)

% Choose default command line output for PolarMeasure
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global data2;
global data;
data2.id_loaded=0;
data2.ir_loaded=0;
data2.sig_loaded=0;
data.connect=0;
%delete(instrfindall)
%delete(instrfind({'Port'},{get(handles.text_port, 'String')}))



% UIWAIT makes PolarMeasure wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PolarMeasure_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;





function text_port_Callback(hObject, eventdata, handles)
% hObject    handle to text_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_port as text
%        str2double(get(hObject,'String')) returns contents of text_port as a double


% --- Executes during object creation, after setting all properties.
function text_port_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in button_connect.
function button_connect_Callback(hObject, eventdata, handles)
% hObject    handle to button_connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
set(handles.text_info,'String','p�ipojuji...')
a=arduino(get(handles.text_port, 'String'));
if exist('a','var') && isa(a,'arduino') && isvalid(a),
    set(handles.text_status, 'String','p�ipojeno')
    set(handles.text_status, 'ForegroundColor','green')
    set(handles.text_info,'String','Arduino bylo �spe�n� p�ipojeno.')
    a.pinMode(2,'output');
    a.pinMode(3,'output');
    a.pinMode(4,'output');
    a.digitalWrite(4,1);
    data.connect=1;
else
    set(handles.text_info,'String','Spojen� nebylo nav�z�no.')
end
data.pozice=0;
data.rychlost=0.007;



% --- Executes on button press in button_disconnect.
function button_disconnect_Callback(hObject, eventdata, handles)
% hObject    handle to button_disconnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
data.connect=0;
a.digitalWrite(2,0);
a.digitalWrite(3,0);
a.digitalWrite(4,0);
delete(instrfind({'Port'},{get(handles.text_port, 'String')}))
set(handles.text_status, 'String','odpojeno')
set(handles.text_status, 'ForegroundColor','red')
set(handles.text_info,'String','Arduino bylo �spe�n� odpojeno.')




% --- Executes on button press in button_test.
function button_test_Callback(hObject, eventdata, handles)
% hObject    handle to button_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
data.rychlost=get(handles.check_rychlost,'Value')/100000;
if data.connect==1
    set(handles.text_info,'String','Prob�h� test motoru.')
    pause(0.1);
    a.digitalWrite(2,0);
    for k=1:400
        a.digitalWrite(3,1);
        pause(data.rychlost);
        a.digitalWrite(3,0);
        pause(data.rychlost);
    end
    data.pozice=data.pozice+400;
    pause(0.5);
    a.digitalWrite(2,1);
    for k=1:400
        a.digitalWrite(3,1);
        pause(data.rychlost);
        a.digitalWrite(3,0);
        pause(data.rychlost);
    end
    data.pozice=data.pozice-400;
    set(handles.text_info,'String',' ')
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end



% --- Executes on button press in button_reset.
function button_reset_Callback(hObject, eventdata, handles)
% hObject    handle to button_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
if data.connect==1
    set(handles.text_info,'String','N�vrat polohy motoru na p�vodn� pozici.')
    pause(0.1);
    if data.pozice<0
        a.digitalWrite(2,0);
        data.pozice=abs(data.pozice);
    else
        a.digitalWrite(2,1);
    end
    posun=mod(data.pozice,1600);
    for k=1:posun
        a.digitalWrite(3,1);
        a.digitalWrite(3,0);
    end
    data.pozice=0;
    set(handles.text_info,'String','Motor byl oto�en do p�vodn� polohy.')
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end


% --- Executes on button press in button_441.
function button_441_Callback(hObject, eventdata, handles)
% hObject    handle to button_441 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of button_441


% --- Executes on button press in button_48.
function button_48_Callback(hObject, eventdata, handles)
% hObject    handle to button_48 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of button_48


% --- Executes on button press in button_custom.
function button_custom_Callback(hObject, eventdata, handles)
% hObject    handle to button_custom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of button_custom



function text_customfreq_Callback(hObject, eventdata, handles)
% hObject    handle to text_customfreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_customfreq as text
%        str2double(get(hObject,'String')) returns contents of text_customfreq as a double


% --- Executes during object creation, after setting all properties.
function text_customfreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_customfreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_testSoundOut.
function button_testSoundOut_Callback(hObject, eventdata, handles)
% hObject    handle to button_testSoundOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
set(handles.text_info,'String','Prob�h� test zvukov�ho v�stupu.')
pause(0.1);
switch get(get(handles.panel_fs,'SelectedObject'),'Tag');
            case 'button_441', data.fs=44100;
            case 'button_48', data.fs=48000;
            case 'button_custom', data.fs=str2num(get(handles.text_customfreq,'String'))*1000;
end
test=generate_sinesweeps(200,22000,data.fs,str2num(get(handles.text_N,'String')));
soundsc(test,data.fs);
pause(ceil(length(test/44100)));
set(handles.text_info,'String',' ')



function text_N_Callback(hObject, eventdata, handles)
% hObject    handle to text_N (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_N as text
%        str2double(get(hObject,'String')) returns contents of text_N as a double


% --- Executes during object creation, after setting all properties.
function text_N_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_N (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_ir_korekce.
function button_ir_korekce_Callback(hObject, eventdata, handles)
% hObject    handle to button_ir_korekce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data2;
[nazev_ir,path_ir] = uigetfile({'*.mat'},'File Selector');
full_path_ir = strcat(path_ir, nazev_ir);
data2.ir=importdata(full_path_ir);
set(handles.text_ir_nazev, 'String', nazev_ir)
data2.ir_loaded=1;



% --- Executes on button press in button_read_signal.
function button_read_signal_Callback(hObject, eventdata, handles)
% hObject    handle to button_read_signal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data2;
[nazev_sig, path_sig] = uigetfile({'*.wav'},'File Selector');
full_path_sig = strcat(path_sig, nazev_sig);
data2.sig = audioread(full_path_sig);
set(handles.text_signal_nazev, 'String', nazev_sig)
data2.sig_loaded=1;


% --- Executes on button press in button_load_id.
function button_load_id_Callback(hObject, eventdata, handles)
% hObject    handle to button_load_id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data2;
[nazev_id, path_id] = uigetfile({'*.mat'},'File Selector');
full_path_id = strcat(path_id, nazev_id);
data2.id=importdata(full_path_id);
set(handles.text_idfile, 'String', nazev_id)
data2.id_loaded=1;


% --- Executes on button press in button_analyze.
function button_analyze_Callback(hObject, eventdata, handles)
% hObject    handle to button_analyze (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data2;
if data2.id_loaded==1 %kontrola na�ten� id filu
    if data2.sig_loaded==1 %kontrola na�ten� sign�lu ("if" je dvojit� kv�li rozd�ln�m varovn�m hl�k�m)
        delka=sum(data2.id.cas(1:(360/data2.id.krok)))*data2.id.fs+length(data2.id.sync_sum)-2*data2.id.fs; %teoreticka minimalni delka signalu - soucet vsech casu+delka pocatecniho sumu(resp.delka mezery mezi sumem a prvni signalem - je stejne dlouha jako sum samy)...a 2s rezerva
        rv = length(data2.sig)-delka; %rozdil vzorku
        z_sig = data2.sig(1:rv); %zkr�cen� sign�l na maxim�ln� mo�n� �as na za��tku (za p�edpokladu �e bylo dom��eno) za ��elem minimalizace po�etn�ho �asu p�i korelaci
        [hodnota,pozice] = max(xcorr(data2.id.sync_sum,z_sig)); %zji�t�n� maxima korelace
        pos=abs(rv-pozice);
        s_sig=data2.sig(pos+2*length(data2.id.sync_sum):end); %srovnan� sign�l
        
        if data2.id.metoda==1
            sweep=generate_sinesweeps2(100,22000,data2.id.fs,data2.id.N,data2.id.repetitions);
            start=1;
            for i=1:(360/data2.id.krok)
                if i==1
                    window=s_sig(start:ceil(length(sweep)+0.25*data2.id.fs-1)); %okno s tolearanc� 0.25s
                    start=start+0.25*data2.id.fs; %kompenzace n�sleduj�c�ho ode�tu
                else
                    window=s_sig(floor(start-0.25*data2.id.fs):ceil((length(sweep)+start+0.25*data2.id.fs-1))); %okno s toleranc� 0.25s na ka�dou stranu + posun o p�edchoz�
                end
                window_temp=window(1:length(sweep));
                [hodnota,pozice]=max(xcorr(sweep,window_temp));
                pos1=abs(length(sweep)-pozice);
                start=start+pos1+data2.id.cas(i)*data2.id.fs-0.25*data2.id.fs;
                window_sync=window(pos1:pos1+length(sweep)-1);
                h=ifft(fft([window_sync zeros(2^ceil(log2(length(sweep)))-length(sweep),1)])./fft([sweep' zeros(2^ceil(log2(length(sweep)))-length(sweep),1)])); %v�po�et IR
                h=[h(ceil(length(h)/2):end,1)' h(1:(ceil(length(h)/2))-1,1)']'; %vycentrov�n� IR

%                 filter1=compute_inverse_filter(sweep, 100, 20000, 44100);
%                 h=conv(window_sync, filter1)';
                if i==1
                    set(gcf,'currentch',char(1)) % "reset" bufferu s posledn�m charem (enter)
                    axes(handles.axes_ir);
                    plot(h); title('Zazoomujte v IR, aby byla videt IR az po prvn� odraz - potvrdte Enterem');
                    zoom on;
                    waitfor(gcf,'CurrentCharacter',13)% �ek�n� na char - enter
                    zoom reset
                    zoom off
                    title('Kliknete na zacatek IR');
                    [x_start]=ginput(1);
                    title('Kliknete pred prvni odraz');
                    [x_stop]=ginput(1);
                    title('Impulsov� odezva na 0�');
                    [max1,x_max]=max(h);
                    x_dif1=x_stop-x_start;
                    x_dif=x_max-x_start;
                    four=abs(fft(h(round(x_start):round(x_stop),1),data2.id.fs));
                    fr=mag2db(abs(four(2:data2.id.fs/2)));
                    data2.fr(i,:)=fr;
                    axes(handles.axes_fr);
                    semilogx(data2.fr(i,:))
                    title('Frekven�n� charakteristika p�ed korekc�');
                else
                    ii=0;
                    x_start=round(x_start(1,1));
                    [max1, x_max1]=max(h);
                    four=abs(fft(h(x_max1-x_dif:x_max1-x_dif+x_dif1,1),data2.id.fs));
                    fr=mag2db(abs(four(2:data2.id.fs/2)));
                    data2.fr(i,:)=fr;
                end
            end
        end
        if data2.id.metoda==2
            if data2.id.repetitions==1
                signal=GenerateMLS(data2.id.N,1);
                mls=signal;
            else
                [signal mls] =GenerateMLSSequence(data2.id.repetitions,data2.id.N,1);                
            end
            mlss=mls;
            for repe=1:data2.id.repetitions+1
                 mlss=[mlss mls];
            end
            start=1;
            for i=1:(360/data2.id.krok)
                if i==1
                    window=s_sig(start:ceil(length(signal)+0.25*data2.id.fs-1)); %okno s tolearanc� 0.25s
                    start=start+0.25*data2.id.fs; %kompenzace n�sleduj�c�ho ode�tu
                else
                    if i==(360/data2.id.krok)
                        window=s_sig(floor(start-0.25*data2.id.fs):end);
                    else
                        window=s_sig(floor(start-0.25*data2.id.fs):ceil((length(signal)+start+0.25*data2.id.fs-1))); %okno s tolearanc� 0.25s na ka�dou stranu + posun o p�edchoz�
                    end
                end
                window_temp=window(1:length(signal));
                [hodnota,pozice]=max(xcorr(signal,window_temp));
                pos1=abs(length(signal)-pozice);
                start=start+pos1+data2.id.cas(i)*data2.id.fs-0.25*data2.id.fs;
                window_sync=window(pos1:pos1+length(signal)-1);
                h=xcorr(window_sync,mlss);
                if i==1
                    set(gcf,'currentch',char(1)) % "reset" bufferu s posledn�m charem (enter)
                    axes(handles.axes_ir);
                    plot(h); title('Zazoomujte v IR, aby byla videt IR az po prvn� odraz - potvrdte Enterem');
                    zoom on;
                    waitfor(gcf,'CurrentCharacter',13)% �ek�n� na char - enter
                    zoom reset
                    zoom off
                    title('Kliknete na zacatek IR');
                    [x_start]=ginput(1); 
                    title('Kliknete pred prvni odraz');
                    [x_stop]=ginput(1); 
                    title('Impulsov� odezva na 0�');
                    four=abs(fft(h(round(x_start):round(x_stop),1),data2.id.fs));
                    fr=mag2db(abs(four(2:data2.id.fs/2)));
                    data2.fr(i,:)=fr;
                    axes(handles.axes_fr);
                    semilogx(data2.fr(i,:))
                    title('Frekven�n� charakteristika p�ed korekc�');
                else
                    four=abs(fft(h(round(x_start):round(x_stop),1),data2.id.fs));
                    fr=mag2db(abs(four(2:data2.id.fs/2)));
                    data2.fr(i,:)=fr;
                end
            end
        end
        if data2.ir_loaded==1
            four=abs(fft(data2.ir,data2.id.fs));
            fr_z=mag2db(abs(four(2:data2.id.fs/2)));
            for i=1:(360/data2.id.krok)
                data2.fr(i,:)=data2.fr(i,:)+fr_z';
            end
            axes(handles.axes_fr2);
            semilogx(data2.fr(1,:));
            title('Frekven�n� charakteristika po korekci');
        end
        if isfield(data2, 'f')==1
            data2=rmfield(data2,'f');%vymaz�n� f, aby nez�stavaly frekvence z p�edchoz� anal�zy
        end
        if isfield(data2, 'vk')==1
            data2=rmfield(data2,'vk');%vymaz�n� vk
        end
        i=1;
        if get(handles.check_f1, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f1,'String'));
            i=i+1;
        end
        if get(handles.check_f2, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f2,'String'));
            i=i+1;
        end
        if get(handles.check_f3, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f3,'String'));
            i=i+1;
        end
        if get(handles.check_f4, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f4,'String'));
            i=i+1;
        end
        if get(handles.check_f5, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f5,'String'));
            i=i+1;
        end
        if get(handles.check_f6, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f6,'String'));
            i=i+1;
        end
        if get(handles.check_f7, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f7,'String'));
            i=i+1;
        end
        if get(handles.check_f8, 'Value')==1
            data2.f(i)=str2num(get(handles.text_f8,'String'));
            i=i+1;
        end
        data2.pf=i-1; %po�et frekvenc�
        data2.theta= -180:data2.id.krok:180;
        data2.cc=hsv(data2.pf);
        for k=1:360/data2.id.krok
            for i=1:data2.pf
                data2.vk(k,i)=(data2.fr(k,data2.f(i))); %dvojit� for cyklus - do vk (vybrane koeficienty) se zapisuje vzdy prislusna frekvence (viz f(i)) pro prislusny uhel k
            end            
        end
        data2.vk((360/data2.id.krok)+1,:)=data2.vk(1,:);% na posledn� azimut+1 (360�), je t�eba zkop�rovat �daj z 0�
        data2.romin=floor(min(min(data2.vk))/10)*10; %minimum zaokrouhleno na des�tky dol�
        data2.romax=ceil(max(max(data2.vk))/10)*10; %maximum zaokrouhleno na des�tky nahoru
        
%         data2.romin=-20; %mo�nost ru�n�ho nastaven� rozsahu
%         data2.romax=10;
%         data2.cc(1,:)=[0.92941 0.10588 0.14118]; %mo�nost ru�n�ho nastaven� barev
%         data2.cc(2,:)=[0 0.72549 0.94902];
%         data2.cc(3,:)=[0 0.55294 0.29412];
%         data2.cc(4,:)=[0.39216 0.19608 0.39216];
        
        data2.roticks=floor(abs(data2.romin-data2.romax)/10);
        axes(handles.axes_polar);
        for k=1:data2.pf
            p(k)=Dirplot(data2.theta,(data2.vk(:,k))','-r',[data2.romax data2.romin data2.roticks]);
            hold on
            set(p(k),'color',data2.cc(k,:),'linewidth',3)
        end
        data2.str = strtrim(cellstr(int2str(data2.f.')));
        legg=legend(data2.str{:});
        set(legg,'Location','NorthEastOutside')
        title(['Smerova charakteristika mikrofonu s krokem ', num2str(data2.id.krok), '�']);
        hold off
                
    else
        warndlg('Na�t�te pros�m nam��en� sign�l.','Nejsou na�teny pot�ebn� soubory')
    end    
else
    warndlg('Na�t�te pros�m informa�n� matici.','Nejsou na�teny pot�ebn� soubory')
end
    



% --- Executes on button press in button_mereni.
function button_mereni_Callback(hObject, eventdata, handles)
% hObject    handle to button_mereni (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
if data.connect==1
    set(handles.text_info,'String','Prob�h� m��en�...')
    if get(handles.check_delay,'Value')==1
        pause(str2num(get(handles.text_delay,'String')));
    end
    data.rychlost=get(handles.check_rychlost,'Value')/100000;
    switch get(get(handles.panel_krok,'SelectedObject'),'Tag'); %zji�t�n� kroku
          case 'button_1_8',data.krok=1.8;
          case 'button_3_6',data.krok=3.6;
          case 'button_7_2',data.krok=7.2;
          case 'button_14_4',data.krok=14.4;
    end
    switch get(get(handles.panel_fs,'SelectedObject'),'Tag'); %zji�t�n� fs
          case 'button_441', data.fs=44100;
          case 'button_48', data.fs=48000;
          case 'button_custom', data.fs=str2num(get(handles.text_customfreq,'String'))*1000;
    end 
    data.N=str2num(get(handles.text_N,'String')); %N
    data.repetitions=str2num(get(handles.text_repet,'String'));
    if isfield(data, 'cas')==1
        data=rmfield(data,'cas');%vymaz�n� vk
    end
    switch get(get(handles.panel_metoda,'SelectedObject'),'Tag');%zji�t�n� metody
          case 'button_sweep', %sweep
              data.metoda=1;
              signal=generate_sinesweeps2(100,22000,data.fs,data.N,data.repetitions);
          case 'button_mls', 
              data.metoda=2;
              if data.repetitions==1
                  signal=GenerateMLS(data.N,1);
              else
                  signal=GenerateMLSSequence(data.repetitions,data.N,1);
              end
    end
    k=1;
    data.stop=0;
    a.digitalWrite(2,0);
    data.sync_sum=rand(1*data.fs,1);
    yRange=[-0.5,0.5];
    soundsc(data.sync_sum,data.fs,yRange);
    pause((2*(length(data.sync_sum))/data.fs));
    stoop=0;
    while k<((360/data.krok)+1) && stoop==0
        tic
      soundsc(signal,data.fs);
      pause((length(signal)/data.fs)+0.5);
      for l=1:((data.krok*8)/1.8)
          a.digitalWrite(3,1);
          pause(data.rychlost);
          a.digitalWrite(3,0);
          pause(data.rychlost);
      end
      data.pozice=data.pozice+((data.krok*8)/1.8);
      k=k+1;
      if data.stop==1
          stoop=1;
      end
      data.cas(k-1)=toc;
    end
    if data.stop==1
      set(handles.text_info,'String','M��en� bylo p�eru�eno!')
      if get(handles.check_auto, 'Value')==1
            set(handles.text_info,'String','N�vrat polohy motoru na p�vodn� pozici.')
            pause(0.1);
            if data.pozice<0
                a.digitalWrite(2,0);
                data.pozice=abs(data.pozice);
            else
                a.digitalWrite(2,1);
            end
            posun=mod(data.pozice,1600);
            for k=1:posun
                a.digitalWrite(3,1);
                a.digitalWrite(3,0);
            end
            data.pozice=0;
            set(handles.text_info,'String','Motor byl oto�en do p�vodn� polohy.')
      end
    else
      if get(handles.check_auto, 'Value')==1
            set(handles.text_info,'String','Prob�h� rozmot�v�n� kabelu.')
            pause(0.1);
            a.digitalWrite(2,1);
            for k=1:1600
                    a.digitalWrite(3,1);
                    a.digitalWrite(3,0);
            end
            set(handles.text_info,'String','Kabel byl rozmot�n.')
      end
      [file_name,path] = uiputfile('*.mat');
      full_path = strcat(path, file_name);
      set(handles.text_info,'String',['M��en� bylo dokon�eno, �daje o m��ic�m sign�lu byly zaps�ny do souboru ' file_name ' .'] )
      save(full_path,'data');
    end
else
    warndlg('Pro spu�t�n� m��en� p�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end



% --- Executes on button press in button_abort.
function button_abort_Callback(hObject, eventdata, handles)
% hObject    handle to button_abort (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
data.stop=1;


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in check_fadein.
function check_fadein_Callback(hObject, eventdata, handles)
% hObject    handle to check_fadein (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_fadein



function text_fadein_Callback(hObject, eventdata, handles)
% hObject    handle to text_fadein (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_fadein as text
%        str2double(get(hObject,'String')) returns contents of text_fadein as a double


% --- Executes during object creation, after setting all properties.
function text_fadein_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_fadein (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_fadeout.
function check_fadeout_Callback(hObject, eventdata, handles)
% hObject    handle to check_fadeout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_fadeout



function text_fadeout_Callback(hObject, eventdata, handles)
% hObject    handle to text_fadeout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_fadeout as text
%        str2double(get(hObject,'String')) returns contents of text_fadeout as a double


% --- Executes during object creation, after setting all properties.
function text_fadeout_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_fadeout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f1.
function check_f1_Callback(hObject, eventdata, handles)
% hObject    handle to check_f1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f1



function text_f1_Callback(hObject, eventdata, handles)
% hObject    handle to text_f1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f1 as text
%        str2double(get(hObject,'String')) returns contents of text_f1 as a double


% --- Executes during object creation, after setting all properties.
function text_f1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f2.
function check_f2_Callback(hObject, eventdata, handles)
% hObject    handle to check_f2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f2



function text_f2_Callback(hObject, eventdata, handles)
% hObject    handle to text_f2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f2 as text
%        str2double(get(hObject,'String')) returns contents of text_f2 as a double


% --- Executes during object creation, after setting all properties.
function text_f2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f3.
function check_f3_Callback(hObject, eventdata, handles)
% hObject    handle to check_f3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f3



function text_f3_Callback(hObject, eventdata, handles)
% hObject    handle to text_f3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f3 as text
%        str2double(get(hObject,'String')) returns contents of text_f3 as a double


% --- Executes during object creation, after setting all properties.
function text_f3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f4.
function check_f4_Callback(hObject, eventdata, handles)
% hObject    handle to check_f4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f4



function text_f4_Callback(hObject, eventdata, handles)
% hObject    handle to text_f4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f4 as text
%        str2double(get(hObject,'String')) returns contents of text_f4 as a double


% --- Executes during object creation, after setting all properties.
function text_f4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f5.
function check_f5_Callback(hObject, eventdata, handles)
% hObject    handle to check_f5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f5



function text_f5_Callback(hObject, eventdata, handles)
% hObject    handle to text_f5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f5 as text
%        str2double(get(hObject,'String')) returns contents of text_f5 as a double


% --- Executes during object creation, after setting all properties.
function text_f5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f6.
function check_f6_Callback(hObject, eventdata, handles)
% hObject    handle to check_f6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f6



function text_f6_Callback(hObject, eventdata, handles)
% hObject    handle to text_f6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f6 as text
%        str2double(get(hObject,'String')) returns contents of text_f6 as a double


% --- Executes during object creation, after setting all properties.
function text_f6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f7.
function check_f7_Callback(hObject, eventdata, handles)
% hObject    handle to check_f7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f7



function text_f7_Callback(hObject, eventdata, handles)
% hObject    handle to text_f7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f7 as text
%        str2double(get(hObject,'String')) returns contents of text_f7 as a double


% --- Executes during object creation, after setting all properties.
function text_f7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_f8.
function check_f8_Callback(hObject, eventdata, handles)
% hObject    handle to check_f8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_f8



function text_f8_Callback(hObject, eventdata, handles)
% hObject    handle to text_f8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_f8 as text
%        str2double(get(hObject,'String')) returns contents of text_f8 as a double


% --- Executes during object creation, after setting all properties.
function text_f8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_f8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function text_repet_Callback(hObject, eventdata, handles)
% hObject    handle to text_repet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_repet as text
%        str2double(get(hObject,'String')) returns contents of text_repet as a double


% --- Executes during object creation, after setting all properties.
function text_repet_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_repet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_rychlost_Callback(hObject, eventdata, handles)
% hObject    handle to slider_rychlost (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global data;
data.rychlost=get(handles.slider_rychlost,'Value');

% --- Executes during object creation, after setting all properties.
function slider_rychlost_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_rychlost (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function text_rychlost_Callback(hObject, eventdata, handles)
% hObject    handle to text_rychlost (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_rychlost as text
%        str2double(get(hObject,'String')) returns contents of text_rychlost as a double


% --- Executes during object creation, after setting all properties.
function text_rychlost_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_rychlost (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_rychlost.
function check_rychlost_Callback(hObject, eventdata, handles)
% hObject    handle to check_rychlost (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_rychlost



% --- Executes on button press in button_kabel.
function button_kabel_Callback(hObject, eventdata, handles)
% hObject    handle to button_kabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global a;
global data;
if data.connect==1
    set(handles.text_info,'String','Prob�h� rozmot�v�n� kabelu.')
    pause(0.1);
        a.digitalWrite(2,1);
        for k=1:1600
                a.digitalWrite(3,1);
                a.digitalWrite(3,0);
        end
        set(handles.text_info,'String','Kabel byl rozmot�n.')
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end


% --- Executes on button press in button_save.
function button_save_Callback(hObject, eventdata, handles)
% hObject    handle to button_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data2;
[file_name,path] = uiputfile('*.pdf');
full_path = strcat(path, file_name);
f2=figure;
for k=1:data2.pf
    p(k)=Dirplot(data2.theta,(data2.vk(:,k))','-r',[data2.romax data2.romin data2.roticks]);
    hold on
    set(p(k),'color',data2.cc(k,:),'linewidth',3)
end
data2.str = strtrim(cellstr(int2str(data2.f.')));
% legg=legend(strcat(num2str(data2.f(1)), ' Hz'),strcat(num2str(data2.f(2)), ' Hz'),strcat(num2str(data2.f(3)), ' Hz'),strcat(num2str(data2.f(4)), ' Hz'),strcat(num2str(data2.f(5)), ' Hz'),strcat(num2str(data2.f(6)), ' Hz'),strcat(num2str(data2.f(7)), ' Hz'),strcat(num2str(data2.f(8)), ' Hz'));
legg=legend(data2.str{:});
set(legg,'Location','NorthEastOutside')
% title(['Smerova charakteristika mikrofonu s krokem ', num2str(data2.id.krok), '�']);
hold off
saveas(f2,full_path,'pdf');
close(f2)




% --- Executes on button press in check_delay.
function check_delay_Callback(hObject, eventdata, handles)
% hObject    handle to check_delay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_delay



function text_delay_Callback(hObject, eventdata, handles)
% hObject    handle to text_delay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_delay as text
%        str2double(get(hObject,'String')) returns contents of text_delay as a double


% --- Executes during object creation, after setting all properties.
function text_delay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_delay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check_auto.
function check_auto_Callback(hObject, eventdata, handles)
% hObject    handle to check_auto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check_auto


% --- Executes on button press in button_replot.
function button_replot_Callback(hObject, eventdata, handles)
% hObject    handle to button_replot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
