load('export_prrocessed_data.mat');
i_celk=1;
names_ele=fieldnames(processed_data);
for i_ele=1:length(names_ele)
   eval(strcat('names_az=fieldnames(processed_data.',names_ele{i_ele},');'));
   for i_az=1:length(names_az)
       eval(strcat('IRs(i_celk,:)=processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut;')); %do processed_data se nacpou o��znut� nefiltrovan� IRky
       eval(strcat('IRs_r(i_celk,:)=processed_data.',names_ele{i_ele},'.',names_az{i_az},'.right_cut;')); %do processed_data se nacpou o��znut� nefiltrovan� IRky
       i_celk=i_celk+1; 
   end
end

filtrace=0;
%% filtrace rawIR
if filtrace==1
    for i=1:i_celk-1
        Hd = lowpass_filter(96000,18000,19000);
        processed = filter(Hd,IRs(i,:));
        Hd = highpass_filter(96000,100,80);
        IRs(i,:) = filter(Hd,processed);
        
        Hd = lowpass_filter(96000,18000,19000);
        processed = filter(Hd,IRs_r(i,:));
        Hd = highpass_filter(96000,100,80);
        IRs_r(i,:) = filter(Hd,processed);
    end
end
%% kreslen� p�r p��klad� IR
subplot(411); plot(IRs(10,:));
subplot(412); plot(IRs(40,:));
subplot(413); plot(IRs(60,:));
subplot(414); plot(IRs(80,:));

%%
for i=1:i_celk-1
    TFs(i,:)=fft(IRs(i,:));  
    TFs_r(i,:)=fft(IRs_r(i,:));  
end
TF_sum=(1/(i_celk-1))*sum(TFs,1);
TF_r_sum=(1/(i_celk-1))*sum(TFs_r,1);

for i=1:i_celk-1
    abs_DTFs(i,:)=abs(TFs(i,:))./abs(TF_sum);
    ang_DTFs(i,:)=angle(TFs(i,:))-angle(TF_sum);
    DTFs(i,:)=abs_DTFs(i,:).*exp(1i*ang_DTFs(i,:));
    
    abs_DTFs_r(i,:)=abs(TFs_r(i,:))./abs(TF_r_sum);
    ang_DTFs_r(i,:)=angle(TFs_r(i,:))-angle(TF_r_sum);
    DTFs_r(i,:)=abs_DTFs_r(i,:).*exp(1i*ang_DTFs_r(i,:));
end

%%

for i=1:i_celk-1
    DIRs(i,:)=real(ifft(DTFs(i,:)));
    
    DIRs_r(i,:)=real(ifft(DTFs_r(i,:)));
end

filtrace=0;
if filtrace==1
    for i=1:i_celk-1
        Hd = lowpass_filter(96000,18000,19000);
        processed = filter(Hd,DIRs(i,:));
        Hd = highpass_filter(96000,100,80);
        DIRs(i,:) = filter(Hd,processed);
        
        Hd = lowpass_filter(96000,18000,19000);
        processed = filter(Hd,DIRs_r(i,:));
        Hd = highpass_filter(96000,100,80);
        DIRs_r(i,:) = filter(Hd,processed);
    end
end

for i=1:i_celk-1
    DTFs(i,:)=fft(DIRs(i,:));    
    
    DTFs_r(i,:)=fft(DIRs_r(i,:)); 
end
%%

subplot(221); semilogx(abs(TFs(10,1:length(TFs)/2)));
subplot(222); semilogx(abs(TFs(40,1:length(TFs)/2)));
subplot(223); semilogx(abs(DTFs(10,1:length(DTFs)/2)));
subplot(224); semilogx(abs(DTFs(40,1:length(DTFs)/2)));