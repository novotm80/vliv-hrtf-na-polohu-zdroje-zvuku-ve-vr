mls14=GenerateMLSSequence(4, 14, 1);
mls15=GenerateMLSSequence(4, 15, 1);
mls16=GenerateMLSSequence(4, 16, 1);
%%
ir1=[0 1 2 3 4 5 6 3 0 -2 -3 -1 2 0];
ir2=[0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 0];
ir3=[0 -1 -2 -3 -4 -5 -6 -7 -8 -9 -10 -11 -12 -13 -14 -15 0];

mls14=conv(mls14,ir1);
mls15=conv(mls15,ir2);
mls16=conv(mls16,ir3);
%%

mls_mesh=[mls14; zeros(length(mls16)-length(mls14),1)]+[mls15; zeros(length(mls16)-length(mls15),1)]+mls16;

ir14=AnalyseMLSSequence(mls_mesh,0,4,14,1,1);
ir15=AnalyseMLSSequence(mls_mesh,0,4,15,1,1);
%ir16=AnalyseMLSSequence(mls_mesh,0,4,16,1,1);
subplot(311); plot(ir14);
subplot(312); plot(ir15);
%subplot(313); plot(ir16);