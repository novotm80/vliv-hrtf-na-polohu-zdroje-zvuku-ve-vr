load('OL_SS_3IRs.mat')
[~,x_peak]=max(IR);
delta_T_OL=35595;
in_str='e20_0_320';

%%
%figure(1); plot(IR); hold on; plot([x_peak-1 x_peak],[-abs(y_peak) abs(y_peak)],'-g'); hold off; title('max peak');
elevs=strsplit(in_str,'_');
NIRs=length(elevs);
for i=1:NIRs
   if strcmp(elevs{i}(1),'e')~=1
       elevs{i}=strcat('e',elevs{i});
   end
end
[~, locs]=findpeaks(IR,'MinPeakDistance',delta_T_OL,'NPeaks',NIRs,'SortStr','descend');
figure(2);findpeaks(IR,'MinPeakDistance',delta_T_OL,'NPeaks',3,'SortStr','descend')
xlabel('Cas [vzorky]'); ylabel('Amplituda'); legend('Signal s v�ce IR','Detekovane peaky')
prelook=1000;
locs=sort(locs);
for i=1:NIRs
    eval(strcat('aa.',elevs{i},'=IR(locs(i)-prelook:locs(i)+delta_T_OL-prelook-1);'));
end
