function varargout = HRTFmeas_analyze(varargin)
% HRTFMEAS_ANALYZE MATLAB code for HRTFmeas_analyze.fig
%      HRTFMEAS_ANALYZE, by itself, creates a new HRTFMEAS_ANALYZE or raises the existing
%      singleton*.
%
%      H = HRTFMEAS_ANALYZE returns the handle to a new HRTFMEAS_ANALYZE or the handle to
%      the existing singleton*.
%
%      HRTFMEAS_ANALYZE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HRTFMEAS_ANALYZE.M with the given input arguments.
%
%      HRTFMEAS_ANALYZE('Property','Value',...) creates a new HRTFMEAS_ANALYZE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HRTFmeas_analyze_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HRTFmeas_analyze_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HRTFmeas_analyze

% Last Modified by GUIDE v2.5 09-May-2016 09:17:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HRTFmeas_analyze_OpeningFcn, ...
                   'gui_OutputFcn',  @HRTFmeas_analyze_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HRTFmeas_analyze is made visible.
function HRTFmeas_analyze_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HRTFmeas_analyze (see VARARGIN)

% Choose default command line output for HRTFmeas_analyze
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
clearvars -global
global data;
data.LP=0; %dolni propust
data.HP=0; %horni propust
data.EQ=0; 
data.loadedraw=0; %nactena syrov�� data
data.cutIR=0; %zvolen o�ez IR
data.loadedEQ=0; %na�tena ekvaliza�n� IR
data.plot_ele=1; %index kreslen� elevace
data.plot_az=1; %index kreslen�ho azimutu
data.PL=0; %kreslen� P+L ucha
data.plot_mode=0; %m�d vykreslen�
data.EQshift=0; %posun ekvaliza�n� IR ve vzorc�ch
data.postEQ=0; %kreslen� pre/post ekvalizovan� IR
data.fft_order=1024; %��d fft�ka
load_sounds(handles);

function load_sounds(handles) %nacteni testovacich zvuku
global sounds;
files=dir('test_sounds');


for i=3:length(files)
    eval(strcat('[~,name,~] = fileparts(''',files(i).name,''');')); %oddeleni pripon
    eval(strcat('sounds.',name,'=audioread(''test_sounds/',files(i).name,''');')); %nacteni
end

jmena=fieldnames(sounds); %ziskani jmen z finalni struktury
set(handles.test_sounds,'String',jmena);



% UIWAIT makes HRTFmeas_analyze wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = HRTFmeas_analyze_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global processed_data;
global raw;

if get(handles.just_cutted,'Value')==1
    names_ele=fieldnames(processed_data);
    for i_ele=1:length(names_ele)
        if strncmpi('e',names_ele{i_ele},1)==1 % p�eko�en� metadat
           eval(strcat('names_az=fieldnames(processed_data.',names_ele{i_ele},');'));
           for i_az=1:length(names_az)
               eval(strcat('processed_data.',names_ele{i_ele},'.',names_az{i_az},'=rmfield(processed_data.',names_ele{i_ele},'.',names_az{i_az},',''left'');'));
               eval(strcat('processed_data.',names_ele{i_ele},'.',names_az{i_az},'=rmfield(processed_data.',names_ele{i_ele},'.',names_az{i_az},',''right'');'));
           end
        end
    end
end


processed_data.metadata=raw.raw_data.metadata;

orig_file_name=get(handles.raw_data_name,'String');
index=strfind(orig_file_name,'raw_data')-2;
eval(strcat('save(''processed_data/',orig_file_name(1:index),'_export'',''processed_data'');'));
set(handles.text_stav,'String',strcat('Data exportov�na: processed_data/',orig_file_name(1:index),'_export.mat'));

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in load_raw.
function load_raw_Callback(hObject, eventdata, handles)
% hObject    handle to load_raw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global raw; 
clear raw.raw_data;
global data;
[nazev, path] = uigetfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
raw=load(full_path);
if isfield(raw,'raw_data')==1
    set(handles.raw_data_name,'String',nazev);
    data.loadedraw=1;
    set(handles.raw_button,'BackgroundColor',[51/255 204/255 0]);
    set(handles.text_metoda,'String',raw.raw_data.metadata(2,1));
    data.method=raw.raw_data.metadata(2,1);
    set(handles.text_fs,'String',raw.raw_data.metadata(2,2));
    data.fs=str2double(raw.raw_data.metadata(2,2));
    set(handles.text_N,'String',raw.raw_data.metadata(2,3));
    data.N=str2double(raw.raw_data.metadata(2,3));
    data.repetitions=str2double(raw.raw_data.metadata(2,4));
    %
    set(handles.text_name,'String',raw.raw_data.metadata(2,7));
    set(handles.text_lastname,'String',raw.raw_data.metadata(2,8));
    set(handles.text_age,'String',raw.raw_data.metadata(2,10));
    set(handles.text_gender,'String',char(raw.raw_data.metadata(2,9)));
    set(handles.text_date,'String',raw.raw_data.metadata(2,11));
    %
    set(handles.cutIR,'Enable','Off');
    set(handles.mode1,'Enable','Off');
    set(handles.cutIR_confirm,'Enable','Off');
    set(handles.manual_start,'Enable','Off');
    set(handles.manual_stop,'Enable','Off');
    set(handles.EQ,'Enable','Off');
    set(handles.plotEQ,'Enable','Off');
    set(handles.compute,'BackgroundColor',[204/255 204/255 204/255])
else
    warndlg('Na�tete podporovan� form�t!','Nepodporovan� form�t dat');
    set(handles.raw_button,'BackgroundColor',[204/255 204/255 204/255]);
    data.loadedraw=1;
    set(handles.raw_data_name,'String','');
    set(handles.text_metoda,'String','');
    set(handles.text_fs,'String','');
    set(handles.text_N,'String','');
end


% --- Executes on button press in raw_button.
function raw_button_Callback(hObject, eventdata, handles)
% hObject    handle to raw_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in loadIR.
function loadIR_Callback(hObject, eventdata, handles)
% hObject    handle to loadIR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
global EQ;
if isempty(EQ)~=1
    EQ=rmfield(EQ,fieldnames(EQ));
end

[nazev, path] = uigetfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
load(full_path);
set(handles.loadedIR_name,'String',nazev);
% set(handles.postEQ,'Enable','On');

data.loadedEQ=1;

filtraceEQ;

cuttingEQ;
data.EQ=1;
set(handles.EQ,'Enable','On');
set(handles.plotEQ,'Enable','On');

% set(handles.EQ,'BackgroundColor',[51/255 204/255 0]);



% --- Executes on button press in measureIR.
function measureIR_Callback(hObject, eventdata, handles)
% hObject    handle to measureIR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
global EQ;
if isempty(EQ)~=1
    EQ=rmfield(EQ,fieldnames(EQ));
end
d = dialog('Position',[300 300 250 150],'Name','Instrukce');

txt = uicontrol('Parent',d,...
           'Style','text',...
           'Position',[20 80 210 60],...
           'String',horzcat('Um�st�te mikrofony (lev� - ch1; prav� - ch2) do referen�n� pozice p�ed sestavu reproduktor�, nastavte zes�len� v�ech zesilova�� a stistkn�te OK.'));

btn = uicontrol('Parent',d,...
           'Position',[85 20 70 25],...
           'String','OK',...
           'Callback','delete(gcf)');
uiwait(d);
set(handles.text_stav,'String','Prob�h� m��en� IR soustavy!');
for i=1:6
    sig = generate_sinesweeps(100,20000,data.fs,data.N);
    meaSig  = sig';
    sync = meaSig;
    playDeviceName = 'default';
    recDeviceName = 'default';
    bufferSize = 2048;

    recData = PaR(meaSig*0.9, sync*0.9 ,playDeviceName,i,recDeviceName,[1 2],bufferSize, data.fs);
    EQ.left_orig(:,i)=analyze(recData(:,1));
    EQ.right_orig(:,i)=analyze(recData(:,2));
    pause(1)
end

%%
[nazev, path]=uiputfile(strcat('defaultIRs_',num2str(data.N),'_',num2str(data.fs),'.mat'));
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
save(full_path,'EQ');

set(handles.text_stav,'String','M��en� IR soustavy dokon�eno a ulo�eno!');
set(handles.loadedIR_name,'String',nazev); 
% set(handles.postEQ,'Enable','On');


data.loadedEQ=1;

filtraceEQ;
cuttingEQ;

set(handles.EQ,'Enable','On');
set(handles.plotEQ,'Enable','On');

function filtraceEQ

global EQ;  
global data;
if data.loadedEQ==1
    EQ.left=filtrace(EQ.left_orig);
    EQ.right=filtrace(EQ.right_orig);
end

function cuttingEQ

global EQ;
figure(1);
plot(EQ.left(:,4)); hold on; plot(EQ.right(:,4)); hold off;
set(gcf,'currentch',char(1)) %reset bufferu s charem
zoom on;
title('Zazoomujte na cht�nou IR a stiskn�te Enter');
waitfor(gcf,'CurrentCharacter',char(13))% �ek�n� na char - enter !!! ve starsich verzi Matlabu jen ...cter',13)... zde nutno ...cter',char(13))...
zoom reset
zoom off
title('Klikn�te na za��tek IR');
EQ.x_start=floor(ginput(1));
title('Klikn�te na konec IR');
EQ.x_stop=ceil(ginput(1));
EQ.left_orig_cut=EQ.left_orig(EQ.x_start:EQ.x_stop,:);
EQ.right_orig_cut=EQ.right_orig(EQ.x_start:EQ.x_stop,:);
EQ.left_cut=EQ.left(EQ.x_start:EQ.x_stop,:);
EQ.right_cut=EQ.right(EQ.x_start:EQ.x_stop,:);
plot(EQ.left_cut(:,4)); hold on; plot(EQ.right_cut(:,4)); hold off;
pause(1); close (1)

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1



function window_sam_Callback(hObject, eventdata, handles)
% hObject    handle to window_sam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of window_sam as text
%        str2double(get(hObject,'String')) returns contents of window_sam as a double
global data;
vzorky=str2double(get(handles.window_sam,'String'));
fs=str2double(get(handles.text_fs,'String'));
refl=vzorky*340/fs;
set(handles.window_m,'String',num2str(refl));
data.x_stop=data.x_start+str2double(get(handles.window_sam,'String'));
vykresleni(handles);


% --- Executes during object creation, after setting all properties.
function window_sam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to window_sam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function window_m_Callback(hObject, eventdata, handles)
% hObject    handle to window_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of window_m as text
%        str2double(get(hObject,'String')) returns contents of window_m as a double
global data;
refl=str2double(get(handles.window_m,'String'));
fs=str2double(get(handles.text_fs,'String'));
vzorky=floor((1/340)*refl*fs);
set(handles.window_sam,'String',num2str(vzorky));
data.x_stop=data.x_start+str2double(get(handles.window_sam,'String'));
vykresleni(handles);


% --- Executes during object creation, after setting all properties.
function window_m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to window_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in manualIR.
function manualIR_Callback(hObject, eventdata, handles)
% hObject    handle to manualIR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of manualIR
if get(handles.manualIR,'Value')==1
   set(handles.window_m,'Enable','Off');
   set(handles.window_sam,'Enable','Off');
else
   set(handles.window_m,'Enable','On');
   set(handles.window_sam,'Enable','On'); 
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in LP.
function LP_Callback(hObject, eventdata, handles)
% hObject    handle to LP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
if data.LP == 0
    set(handles.LP,'BackgroundColor',[51/255 204/255 0]);
    data.LP=1;
else
    set(handles.LP,'BackgroundColor',[204/255 204/255 204/255]);
    data.LP=0;
end

filtraceEQ;
if data.loadedEQ==1 && data.loadedraw==1
    vykresleni(handles);
end


% --- Executes on button press in EQ.
function EQ_Callback(hObject, eventdata, handles)
% hObject    handle to EQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
if data.loadedEQ==1
%     if data.EQ == 0
%         set(handles.EQ,'BackgroundColor',[51/255 204/255 0]);
%         data.EQ=1;
%     else
%         set(handles.EQ,'BackgroundColor',[204/255 204/255 204/255]);
%         data.EQ=0;
%     end
    
    data.postEQ=1;

    ekvalizace(handles);
    set(handles.postEQ,'Enable','On');
    set(handles.postEQ,'Value',1);
    vykresleni(handles);
else
    warndlg('Na�tete korek�n� IR!','Nen� na�tena korek�n� IR');
end




function cutIR_Callback(hObject, eventdata, handles)

global data;
% if data.cutIR == 0
%     set(handles.cutIR,'BackgroundColor',[51/255 204/255 0]);
%     data.cutIR=1;
% else
%     set(handles.cutIR,'BackgroundColor',[204/255 204/255 204/255]);
%     data.cutIR=0;
% end
% set(handles.text_stav,'String','Zazoomujte na za��tek IR a stiskn�te ENTER!');
% zoom on;
% waitfor(gcf,'CurrentCharacter',13)% �ek�n� na char - enter
% zoom reset
% zoom off
% set(handles.text_stav,'String','Klikn�te na za��tek IR');
% [data.x_start,~]=ginput(1)
data.x_start=data.peak-round(150*(96000/data.fs));
data.x_stop=data.x_start+round(str2double(get(handles.window_sam,'String'))*(96000/data.fs));
data.plot_mode=3;
set(handles.mode1,'Enable','On');
set(handles.mode1,'Value',1);
vykresleni(handles);
set(handles.cutIR_confirm,'Enable','On');
set(handles.manual_start,'Enable','On');
set(handles.manual_stop,'Enable','On');




function compute_Callback(hObject, eventdata, handles)

global raw;
global data;
global processed_data;
if isempty(processed_data)~=1
    processed_data=rmfield(processed_data,fieldnames(processed_data));
end
% names_processed=fieldnames(processed_data)
% names_raw=fieldnames(raw.raw_data)
if data.loadedraw==1
    set(handles.text_stav,'String','Po��t�m!');
    pause(0.001) % kv�li v�pisu v��e
    names_ele=fieldnames(raw.raw_data);
    for i_ele=1:length(names_ele)
        if strncmpi('e',names_ele{i_ele},1)==1 % p�eko�en� metadat
           eval(strcat('names_az=fieldnames(raw.raw_data.',names_ele{i_ele},');'));
           for i_az=1:length(names_az)
               if strcmp(data.method,'OL_SS')==1
                   eval(strcat('IRleft=analyze(raw.raw_data.',names_ele{i_ele},'.',names_az{i_az},'.left,handles);')); %nefiltrovane IR
                   eval(strcat('IRright=analyze(raw.raw_data.',names_ele{i_ele},'.',names_az{i_az},'.right,handles);'));
                   delta_T_OL=str2double(raw.raw_data.metadata(2,12));
                   [~,x_peak]=max(IRleft);
                   elevs=strsplit(names_ele{i_ele},'_');
                   NIRs=length(elevs);
                   for i=1:NIRs
                      if strcmp(elevs{i}(1),'e')~=1
                          elevs{i}=strcat('e',elevs{i});
                      end
                   end
                   [~, locs]=findpeaks(IRleft,'MinPeakDistance',delta_T_OL,'NPeaks',NIRs,'SortStr','descend');
                   figure(2);findpeaks(IRleft,'MinPeakDistance',delta_T_OL,'NPeaks',NIRs,'SortStr','descend')
                   pause(1); close(2)
                   prelook=1000; postlook=2000;
                   locs=sort(locs);
                   for i=1:NIRs
                       eval(strcat('raw.rawIR.',elevs{i},'.',names_az{i_az},'.left=IRleft(locs(i)-prelook:locs(i)+postlook-prelook-1);'));
                       eval(strcat('raw.rawIR.',elevs{i},'.',names_az{i_az},'.right=IRright(locs(i)-prelook:locs(i)+postlook-prelook-1);'));
                       eval(strcat('processed_data.',elevs{i},'.',names_az{i_az},'.left=filtrace(raw.rawIR.',elevs{i},'.',names_az{i_az},'.left);')); %filtrovane IR
                       eval(strcat('processed_data.',elevs{i},'.',names_az{i_az},'.right=filtrace(raw.rawIR.',elevs{i},'.',names_az{i_az},'.right);'));
                   end
               else
                   eval(strcat('raw.rawIR.',names_ele{i_ele},'.',names_az{i_az},'.left=analyze(raw.raw_data.',names_ele{i_ele},'.',names_az{i_az},'.left,handles);')); %nefiltrovane IR
                   eval(strcat('raw.rawIR.',names_ele{i_ele},'.',names_az{i_az},'.right=analyze(raw.raw_data.',names_ele{i_ele},'.',names_az{i_az},'.right,handles);'));
                   eval(strcat('processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left=filtrace(raw.rawIR.',names_ele{i_ele},'.',names_az{i_az},'.left);')); %filtrovane IR
                   eval(strcat('processed_data.',names_ele{i_ele},'.',names_az{i_az},'.right=filtrace(raw.rawIR.',names_ele{i_ele},'.',names_az{i_az},'.right);'));
               end
            end
        end
    end
%     names_processed=fieldnames(processed_data);
%     save('processed_data','processed_data');
    data.max_ele=length(fieldnames(raw.rawIR));
%     data.max_ele=i_ele-1; %jedna pozice jsou metadata, tohle je �ist� po�et elevac�
    data.max_az=i_az;
    vykresleni(handles);
    set(handles.pre_ele,'Enable','Off');
    set(handles.pre_az,'Enable','Off'); 
    if data.max_ele==1
       set(handles.next_ele,'Enable','Off'); % n�zvy pol� prvn� �rovn� (elevace) 
    else
       set(handles.next_ele,'Enable','On'); 
    end
    if data.max_az==1
       set(handles.next_az,'Enable','Off'); 
    else
       set(handles.next_az,'Enable','On');  
    end
    set(handles.text_stav,'String','Vypo�teno!');
    set(handles.compute,'BackgroundColor',[51/255 204/255 0])
    set(handles.cutIR,'Enable','On');
else
    warndlg('Na�tete syrov� data!','Nejsou na�tena data ke zpracov�n�');
end

function processed=analyze(clean, handles)
% global raw;
global data;
%% v�po�et IR
if strcmp(data.method,'SweepSine')==1
    processed = sinesweeps_response(clean', data.fs, data.N, 100, 20000, 100);    
elseif strcmp(data.method,'MLS')==1
    processed = AnalyseMLSSequence(clean,0,data.repetitions,data.N,'true',0);
elseif strcmp(data.method,'OL_SS')==1
    [~, inv_filter]=generate_sine_sweep(data.fs,(2^data.N)/data.fs, 100, 20000, 0.01, 1);
    processed=conv(inv_filter,clean);
end

function processed=filtrace(clean)
% global raw;
global data;
%% filtrace
processed=clean;
if data.LP==1
    Hd = lowpass_filter(data.fs,18000,19000);
    processed = filter(Hd,clean);
    if data.HP==1
        Hd = highpass_filter(data.fs,100,80);
        processed = filter(Hd,processed);
    end
else
    if data.HP==1
        Hd = highpass_filter(data.fs,100,80);
        processed = filter(Hd,clean);
    end
end


% disp('Po��t�m...')
%processed=1;


function vykresleni(handles) %az a ele jsou stringy // a15, e20, ...
global data;
global processed_data;
global EQ;
global postEQ;
elevace=fieldnames(processed_data);
eval(strcat('azimuty=fieldnames(processed_data.',elevace{data.plot_ele},');'));
set(handles.text_ele,'String',elevace{data.plot_ele});
set(handles.text_az,'String',azimuty{data.plot_az});
data.max_az=length(azimuty);
if get(handles.postEQ,'Value')==0
    eval(strcat('h=processed_data.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.left;'))
    axes(handles.axes_IR);
    plot(h); hold on; legend off; xlabel('Cas [vzorky]'); ylabel('Amplituda');
    [~,data.peak]=max(abs(h));
    eval(strcat('h2=processed_data.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.right;')); 
    if data.PL==1
        plot(h2,'-r'); legend('left','right');
        xlabel('Cas [vzorky]'); ylabel('Amplituda');
    end
%     if data.loadedEQ==1
%         EQ.left_s=circshift(EQ.left,data.EQshift,1);
%         EQ.right_s=circshift(EQ.right,data.EQshift,1);
%     end
%     if get(handles.plotEQ,'Value')==1
%     %     if data.EQshift<0
%     %         EQ.left_s=[EQ.left(1+data.EQshift:end,:); zeros(data.EQshift,6)];
%     %         EQ.right_s=[EQ.right(1+data.EQshift:end,:); zeros(data.EQshift,6)];
%     %     else
%     %         EQ.left_s=[zeros(data.EQshift,6); EQ.left(1:end-data.EQshift,:)];
%     %         EQ.right_s=[zeros(data.EQshift,6); EQ.right(1:end-data.EQshift,:)];
%     %     end
%         plot(EQ.left_s(:,1),'-c'); legend('left','leftEQ');
%         if data.PL==1
%             plot(EQ.right_s(:,1),'-m'); legend('left','right','leftEQ','rightEQ');
%         end
%     end
    switch data.plot_mode
        case 0 % cela IR
            h_cut=h;
            h2_cut=h2;
%             if data.loadedEQ==1
%                 EQ.left_cut=EQ.left_s;
%                 EQ.right_cut=EQ.right_s;
%             end
            xlabel('Cas [vzorky]'); ylabel('Amplituda');
        case 1 % oriznuta IR
            h_cut=h(data.x_start:data.x_stop);
            h2_cut=h2(data.x_start:data.x_stop);
%             if data.loadedEQ==1
%                 EQ.left_cut=EQ.left_s(data.x_start:data.x_stop,:);
%                 EQ.right_cut=EQ.right_s(data.x_start:data.x_stop,:);
%             end
            axis([data.x_start-30 data.x_stop+100 min([h_cut h2_cut]) max([h_cut h2_cut])]);
            xlabel('Cas [vzorky]'); ylabel('Amplituda');
%         case 2 %oriznuta IR s pocatecni carou
%             h_cut=h(data.x_start:data.x_stop);
%             h2_cut=h2(data.x_start:data.x_stop);
% %             if data.loadedEQ==1
% %                 EQ.left_cut=EQ.left_s(data.x_start:data.x_stop,:);
% %                 EQ.right_cut=EQ.right_s(data.x_start:data.x_stop,:);
% %             end
%             axis([data.x_start-30 data.x_stop+100 min([h_cut h2_cut]) max([h_cut h2_cut])]);
%             plot([data.x_start-1 data.x_start],[-1 1],'-g')
%             xlabel('Cas [vzorky]'); ylabel('Amplituda');
        case 3 %oriznuta IR s dvemi carami
            h_cut=h(data.x_start:data.x_stop);
            h2_cut=h2(data.x_start:data.x_stop);
%             if data.loadedEQ==1
%                 EQ.left_cut=EQ.left_s(data.x_start:data.x_stop,:);
%                 EQ.right_cut=EQ.right_s(data.x_start:data.x_stop,:);
%             end
            axis([data.x_start-30 data.x_stop+100 min(min([h_cut h2_cut])) max(max([h_cut h2_cut]))]);
            plot([data.x_start-1 data.x_start],[min(min([h_cut h2_cut])) max(max([h_cut h2_cut]))],'-g') % kresleni pocatecni cary
            plot([data.x_stop data.x_stop+1],[min(min([h_cut h2_cut])) max(max([h_cut h2_cut]))],'-g') % kresleni koncove cary
            xlabel('Cas [vzorky]'); ylabel('Amplituda');
    end
    
    hold off;
    spectrum=(fft(h_cut,data.fft_order));
    spectrum=spectrum(1:round(end/2));
    real_spectrum=abs(spectrum);
    axes(handles.axes_TF)
    semilogx(0:(data.fs/2)/length(spectrum):data.fs/2-(data.fs/2)/length(spectrum),real_spectrum); axis([100 20000 0 max(real_spectrum(ceil((100*length(spectrum))/(data.fs/2)):ceil((20000*length(spectrum))/(data.fs/2))))]);
    hold on; legend off; 
    xlabel('Frekvence [Hz]'); ylabel('Amplituda');
    if data.PL==1
        spectrum2=(fft(h2_cut,data.fft_order));
        spectrum2=spectrum2(1:round(end/2));
        real_spectrum2=abs(spectrum2);
        semilogx(0:(data.fs/2)/length(spectrum2):data.fs/2-(data.fs/2)/length(spectrum2),real_spectrum2,'-r'); axis([100 20000 0 max(max([real_spectrum2(ceil((100*length(spectrum2))/(data.fs/2)):ceil((20000*length(spectrum2))/(data.fs/2))) real_spectrum(ceil((100*length(spectrum))/(data.fs/2)):ceil((20000*length(spectrum))/(data.fs/2)))]))]); legend('left','right');
        xlabel('Frekvence [Hz]'); ylabel('Amplituda');
    end
    if get(handles.plotEQ,'Value')==1
        spectrumEQ1=(fft(EQ.left_cut(:,1),data.fft_order));
        spectrumEQ1=spectrumEQ1(1:round(end/2));
        real_spectrumEQ1=abs(spectrumEQ1); 
        semilogx(0:(data.fs/2)/length(spectrumEQ1):data.fs/2-(data.fs/2)/length(spectrumEQ1),real_spectrumEQ1,'-c'); 
        legend('left','leftEQ');
        xlabel('Frekvence [Hz]'); ylabel('Amplituda');
        if data.PL==1
            spectrumEQ2=(fft(EQ.right_cut(:,1),data.fft_order));
            spectrumEQ2=spectrumEQ2(1:round(end/2));
            real_spectrumEQ2=abs(spectrumEQ2); 
            semilogx(0:(data.fs/2)/length(spectrumEQ2):data.fs/2-(data.fs/2)/length(spectrumEQ2),real_spectrumEQ2,'-m');
            xlabel('Frekvence [Hz]'); ylabel('Amplituda');
            legend('left','right','leftEQ','rightEQ');
        end
    end
    hold off;
    set(handles.ears,'Enable','On');
else
    axes(handles.axes_IR);
    eval(strcat('h=postEQ.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.left_orig;'))
    plot(h); hold on; xlabel('Cas [vzorky]'); ylabel('Amplituda');
    eval(strcat('h2=postEQ.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.right_orig;'))
    if data.PL==1
        plot(h2,'-r'); legend('left','right');
        xlabel('Cas [vzorky]'); ylabel('Amplituda');
    end
    hold off;
    spectrum=(fft(h,data.fft_order));
    spectrum=spectrum(1:round(end/2));
    real_spectrum=abs(spectrum);
    axes(handles.axes_TF)
    semilogx(0:(data.fs/2)/length(spectrum):data.fs/2-(data.fs/2)/length(spectrum),real_spectrum); axis([100 20000 0 max(real_spectrum(ceil((100*length(spectrum))/(data.fs/2)):ceil((20000*length(spectrum))/(data.fs/2))))]);
    hold on; legend off;
    xlabel('Frekvence [Hz]'); ylabel('Amplituda');
    if data.PL==1
        spectrum2=(fft(h2,data.fft_order));
        spectrum2=spectrum2(1:round(end/2));
        real_spectrum2=abs(spectrum2);
        semilogx(0:(data.fs/2)/length(spectrum2):data.fs/2-(data.fs/2)/length(spectrum2),real_spectrum2,'-r'); axis([100 20000 0 max(max([real_spectrum2(ceil((100*length(spectrum2))/(data.fs/2)):ceil((20000*length(spectrum2))/(data.fs/2))) real_spectrum(ceil((100*length(spectrum))/(data.fs/2)):ceil((20000*length(spectrum))/(data.fs/2)))]))]); legend('left','right');
        xlabel('Frekvence [Hz]'); ylabel('Amplituda');
    end
    hold off;
end


function pre_ele_Callback(hObject, eventdata, handles)

global data;
data.plot_ele=data.plot_ele-1;
data.plot_az=1;
if data.plot_ele~=data.max_ele
   set(handles.next_ele,'Enable','On'); 
end
if data.plot_ele==1
   set(handles.pre_ele,'Enable','Off'); 
end
vykresleni(handles);
set(handles.pre_az,'Enable','Off');
if data.max_az==1
    set(handles.next_az,'Enable','Off');
else
   set(handles.next_az,'Enable','On'); 
end


function pre_az_Callback(hObject, eventdata, handles)

global data;
data.plot_az=data.plot_az-1;
if data.plot_az~=data.max_az;
   set(handles.next_az,'Enable','On'); 
end
if data.plot_az==1
   set(handles.pre_az,'Enable','Off'); 
end
vykresleni(handles);


function next_az_Callback(hObject, eventdata, handles)

global data;
data.plot_az=data.plot_az+1;
if data.plot_az==data.max_az;
   set(handles.next_az,'Enable','Off'); 
end
if data.plot_az~=1
   set(handles.pre_az,'Enable','On'); 
end
vykresleni(handles);



function next_ele_Callback(hObject, eventdata, handles)

global data;
data.plot_ele=data.plot_ele+1;
data.plot_az=1;
if data.plot_ele==data.max_ele
   set(handles.next_ele,'Enable','Off'); 
end
if data.plot_ele~=1
   set(handles.pre_ele,'Enable','On'); 
end
vykresleni(handles);
set(handles.pre_az,'Enable','Off');
if data.max_az==1
    set(handles.next_az,'Enable','Off');
else
   set(handles.next_az,'Enable','On'); 
end



function HP_Callback(hObject, eventdata, handles)

global data;
if data.HP == 0
    set(handles.HP,'BackgroundColor',[51/255 204/255 0]);
    data.HP=1;
else
    set(handles.HP,'BackgroundColor',[204/255 204/255 204/255]);
    data.HP=0;
end
filtraceEQ;
if data.loadedEQ==1 && data.loadedraw==1
    vykresleni(handles);
end


function ears_Callback(hObject, eventdata, handles)

global data;    
if data.PL==0
    data.PL=1;
    set(handles.ears,'String','L');
else
    data.PL=0;
    set(handles.ears,'String','L+P');
end
vykresleni(handles);



function uipanel2_SelectionChangeFcn(hObject, eventdata, handles)

global data;
if get(handles.mode0,'Value')==1
    data.plot_mode=0;
end
if get(handles.mode1,'Value')==1
    data.plot_mode=3;
end
vykresleni(handles);



function manual_start_Callback(hObject, eventdata, handles)

global data;
set(gcf,'currentch',char(1)) %reset bufferu s charem
set(handles.text_stav,'String','Zazoomujte na za��tek IR a stiskn�te ENTER!');
zoom on;
waitfor(gcf,'CurrentCharacter',char(13))% �ek�n� na char - enter !!! ve strarsich verzi Matlabu jen ...cter',13)... zde nutno ...cter',char(13))...
zoom reset
zoom off
set(handles.text_stav,'String','Klikn�te na za��tek IR');
[data.x_start,~]=ginput(1);
vykresleni(handles);
set(handles.text_stav,'String','');



function manual_stop_Callback(hObject, eventdata, handles)

global data;
set(gcf,'currentch',char(1)) %reset bufferu s charem
set(handles.text_stav,'String','Zazoomujte na konec IR a stiskn�te ENTER!');
zoom on;
waitfor(gcf,'CurrentCharacter',char(13))% �ek�n� na char - enter
zoom reset
zoom off
set(handles.text_stav,'String','Klikn�te na konec IR');
[data.x_stop,~]=ginput(1);

vykresleni(handles);
set(handles.text_stav,'String','');


function cutIR_confirm_Callback(hObject, eventdata, handles)

global processed_data;
global data;
global raw;
% global EQ;
names_ele=fieldnames(raw.rawIR);
for i_ele=1:length(names_ele)
   eval(strcat('names_az=fieldnames(raw.rawIR.',names_ele{i_ele},');'));
   for i_az=1:length(names_az)
       eval(strcat('processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut=raw.rawIR.',names_ele{i_ele},'.',names_az{i_az},'.left(data.x_start:data.x_stop);')); %do processed_data se nacpou o��znut� nefiltrovan� IRky
       eval(strcat('processed_data.',names_ele{i_ele},'.',names_az{i_az},'.right_cut=raw.rawIR.',names_ele{i_ele},'.',names_az{i_az},'.right(data.x_start:data.x_stop);'));
       
%        figure(1); eval(strcat('plot(filtrace(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut))'));
%    eval(strcat('size(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut)'))
%    EQ.left_origs=circshift(EQ.left_orig,data.EQshift,1); %posun
%    EQ.right_origs=circshift(EQ.right_orig,data.EQshift,1);   
%    EQ.left_cut=EQ.left_origs(data.x_start:data.x_stop,:); %�e�u nefiltrovan� ekvaliza�n� IR
%    EQ.right_cut=EQ.right_origs(data.x_start:data.x_stop,:);
   
   end
end

set(handles.text_stav,'String','IR o��znuty a zaps�ny do pol� left_cut a right_cut');


function ekvalizace(handles)
global data;
global processed_data;
global postEQ;
global EQ;
% EQ.left_cut;
if isempty(postEQ)~=1
    postEQ=rmfield(postEQ,fieldnames(postEQ));
end
set(handles.text_stav,'String','Ekvalizuji!');
pause(0.001) % kv�li v�pisu v��e
names_ele=fieldnames(processed_data)
for i_ele=1:length(names_ele)
    current_ele=names_ele{i_ele};
    if strncmpi('e',names_ele{i_ele},1)==1 % p�eko�en� metadat
       eval(strcat('names_az=fieldnames(processed_data.',names_ele{i_ele},');'));
       for i_az=1:length(names_az)
           switch current_ele
               case {'e60', 'e70'}
                   ch=1;
               case {'e40', 'e50'}
                   ch=2;
               case {'e20', 'e30'}
                   ch=3;
               case {'e0', 'e10'}
                   ch=4;
               case {'e340', 'e350'}
                   ch=5;
               case {'e320', 'e330'}
                   ch=6;
           end
%            spec_abs_equalizedIR=(spec_cut_rawIR./spec_cut_rawEQ);
%            spec_angle_equalizedIR=spec_angle_cut_rawIR-spec_angle_cut_rawEQ;
%            spec_equalizedIR=spec_abs_equalizedIR.*exp(1i*spec_angle_equalizedIR);
%            equalizedIR=real(ifft(spec_equalizedIR));

if get(handles.pre_filter,'Value')==1 % prefiltrace
    eval(strcat('S_IR.left=fft(filtrace(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut),data.fft_order);'));%spektrum s filtrace
    eval(strcat('S_IR.right=fft(filtrace(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.right_cut),data.fft_order);'));
    
    S_EQ.left=fft(EQ.left_cut(:,ch),data.fft_order); %spektrum EQ filtrovane
    S_EQ.right=fft(EQ.right_cut(:,ch),data.fft_order);
else
    eval(strcat('S_IR.left=fft(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut,data.fft_order);'));%spektrum
    eval(strcat('S_IR.right=fft(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.right_cut,data.fft_order);'));
    
    S_EQ.left=fft(EQ.left_orig_cut(:,ch),data.fft_order); %spektrum EQ
    S_EQ.right=fft(EQ.right_orig_cut(:,ch),data.fft_order);
end
            absS_IR.left=abs(S_IR.left);%abs spektra
            absS_IR.right=abs(S_IR.right);
            % figure(1); eval(strcat('plot(filtrace(processed_data.',names_ele{i_ele},'.',names_az{i_az},'.left_cut));'));
%                     figure(1); plot(absS_IR.left);
            angS_IR.left=angle(S_IR.left);%angle spektra
            angS_IR.right=angle(S_IR.right);
            
            absS_EQ.left=abs(S_EQ.left)'; %abs spektra
            absS_EQ.right=abs(S_EQ.right)';
%                     figure(2); plot(absS_EQ.left);
            angS_EQ.left=angle(S_EQ.left)';%angle spektra
            angS_EQ.right=angle(S_EQ.right)';
            size(absS_IR.left)
            size(absS_EQ.left)
            absS_EQIR.left=absS_IR.left./absS_EQ.left; %d�len� spekter
            absS_EQIR.right=absS_IR.right./absS_EQ.right;
%                     figure(3); plot(absS_EQIR.left);
            
            angS_EQIR.left=angS_IR.left-angS_EQ.left; %od��t�n� f�z�
            angS_EQIR.right=angS_IR.right-angS_EQ.right; 
            
            S_EQIR.left=absS_EQIR.left.*exp(1i*angS_EQIR.left); %spojen�
            S_EQIR.right=absS_EQIR.right.*exp(1i*angS_EQIR.right);
            
            EQIR.left=real(ifft(S_EQIR.left)); %do �asov� dom�ny
            EQIR.right=real(ifft(S_EQIR.right));    
            
            if get(handles.post_filter,'Value')==0 % postfiltrace

                        eval(strcat('postEQ.',names_ele{i_ele},'.',names_az{i_az},'.left_orig=EQIR.left;')); %nefiltrovane
                        eval(strcat('postEQ.',names_ele{i_ele},'.',names_az{i_az},'.right_orig=EQIR.right;'));
            else
                        EQIR.left=filtrace(EQIR.left); %filtrace
                        EQIR.right=filtrace(EQIR.right);

                        eval(strcat('postEQ.',names_ele{i_ele},'.',names_az{i_az},'.left_orig=EQIR.left;')); %filtrovane
                        eval(strcat('postEQ.',names_ele{i_ele},'.',names_az{i_az},'.right_orig=EQIR.right;'));
            end
       end
    end
end
set(handles.text_stav,'String','IR ekvalizov�ny!');


function plotEQ_Callback(hObject, eventdata, handles)

vykresleni(handles);


function loadIR_CreateFcn(hObject, eventdata, handles)



function EQshift_Callback(hObject, eventdata, handles)

global data;
data.EQshift=str2double(get(handles.EQshift,'String'));
vykresleni(handles);


function EQshift_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function postEQ_Callback(hObject, eventdata, handles)

global data;
if get(handles.preEQ,'Value')==1
    data.postEQ=0;
else
    data.postEQ=1;
end
vykresleni(handles);


function test_sounds_Callback(hObject, eventdata, handles)



function test_sounds_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in konvoluce.
function konvoluce_Callback(hObject, eventdata, handles)
% hObject    handle to konvoluce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global sounds;
global data;
global processed_data;
global postEQ;

nazvy=get(handles.test_sounds,'String');
curr_sound = nazvy{get(handles.test_sounds,'Value')};

eval(strcat('test_sound=sounds.',curr_sound,';'));

elevace=fieldnames(processed_data);
eval(strcat('azimuty=fieldnames(processed_data.',elevace{data.plot_ele},');'));
switch get(handles.phase,'Value');
    case 1 %full IR
        eval(strcat('h(:,1)=processed_data.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.left;')) %bere o�ezan� sign�ly
        eval(strcat('h(:,2)=processed_data.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.right;'))
    case 2 %cut IR
        eval(strcat('h(:,1)=processed_data.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.left_cut;')) %bere o�ezan� sign�ly
        eval(strcat('h(:,2)=processed_data.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.right_cut;'))
    case 3 %postEQ IR
        eval(strcat('h(:,1)=postEQ.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.left_orig;')) %bere o�ezan� sign�ly
        eval(strcat('h(:,2)=postEQ.',elevace{data.plot_ele},'.',azimuty{data.plot_az},'.right_orig;'))
end

if get(handles.switch_lr,'Value')==1 %p�ehozen� kan�l� lev�/prav�
    h=circshift(h,1,2);
end
[~,n]=size(test_sound);
if n==1 %v p��pad� mono sign�lu roz���en� do dvou sloupc�
    test_sound(:,2)=test_sound(:,1);
end
convolved(:,1)=conv(test_sound(:,1),resample(h(:,1),44100,data.fs));
convolved(:,2)=conv(test_sound(:,2),resample(h(:,2),44100,data.fs));
soundsc(convolved,44100);
range(convolved)
player = audioplayer(convolved,44100);
play(player);


% --- Executes on button press in switch_lr.
function switch_lr_Callback(hObject, eventdata, handles)
% hObject    handle to switch_lr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of switch_lr


% --- Executes on selection change in phase.
function phase_Callback(hObject, eventdata, handles)
% hObject    handle to phase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns phase contents as cell array
%        contents{get(hObject,'Value')} returns selected item from phase


% --- Executes during object creation, after setting all properties.
function phase_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pre_filter.
function pre_filter_Callback(hObject, eventdata, handles)
% hObject    handle to pre_filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pre_filter


% --- Executes on button press in post_filter.
function post_filter_Callback(hObject, eventdata, handles)
% hObject    handle to post_filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of post_filter


% --- Executes on button press in just_cutted.
function just_cutted_Callback(hObject, eventdata, handles)
% hObject    handle to just_cutted (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of just_cutted
