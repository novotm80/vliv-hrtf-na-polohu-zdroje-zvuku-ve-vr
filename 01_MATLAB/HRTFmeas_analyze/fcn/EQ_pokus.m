load('preEQ_L.mat')
load('preEQ_R.mat')
load('EQ_L.mat')
load('EQ_R.mat')
fs=96000;
%% casovky
figure(1)
subplot(411)
plot(pL); title('L ear')
subplot(412)
plot(pR); title('R ear')
subplot(413)
plot(EQL); title('L EQ')
subplot(414)
plot(EQR); title('R EQ')
%% spektra
figure(2)
subplot(411)
fftcko(pL,fs); axis([100 20000 0 0.3]); title('L ear')
subplot(412)
fftcko(pR,fs); axis([100 20000 0 0.3]); title('R ear')
subplot(413)
fftcko(EQL,fs); axis([100 20000 0 0.3]); title('L EQ')
subplot(414)
fftcko(EQR,fs); axis([100 20000 0 0.3]); title('R EQ')
%% ekvalizovane
fL=ifft(fft(pL)./fft(EQL));
fR=ifft(fft(pR)./fft(EQR));
% fL=filter(EQL,1,pL);
% fR=filter(EQR,1,pR);
figure(3)
subplot(221)
plot(fL)
subplot(222)
plot(fR)
subplot(223)
fftcko(fL,fs); axis([100 20000 0 6])
subplot(224)
% plot(abs(fft(pR)./fft(EQR)))
fftcko(fR,fs); axis([100 20000 0 6])
%% filtrace
Hd=lowpass_filter(fs,18000,19000);
f_fL=filter(Hd,fL);
f_fR=filter(Hd,fR);
figure(4)
subplot(221)
plot(f_fL)
subplot(222)
plot(f_fR)
subplot(223)
fftcko(f_fL,fs); axis([100 20000 0 6])
subplot(224)
fftcko(f_fR,fs); axis([100 20000 0 6])