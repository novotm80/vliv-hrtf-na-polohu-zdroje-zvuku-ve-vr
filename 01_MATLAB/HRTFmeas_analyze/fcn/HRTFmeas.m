   function varargout = HRTFmeas(varargin)
% HRTFMEAS MATLAB code for HRTFmeas.fig
%      HRTFMEAS, by itself, creates a new HRTFMEAS or raises the existing
%      singleton*.
%
%      H = HRTFMEAS returns the handle to a new HRTFMEAS or the handle to
%      the existing singleton*.
%
%      HRTFMEAS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HRTFMEAS.M with the given input arguments.
%
%      HRTFMEAS('Property','Value',...) creates a new HRTFMEAS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HRTFmeas_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HRTFmeas_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HRTFmeas

% Last Modified by GUIDE v2.5 05-May-2016 11:39:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HRTFmeas_OpeningFcn, ...
                   'gui_OutputFcn',  @HRTFmeas_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HRTFmeas is made visible.
function HRTFmeas_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HRTFmeas (see VARARGIN)

% Choose default command line output for HRTFmeas
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global info;
info.connected=0;
info.pozice=0;
info.elevace=[1 1 1 1 1 1];
info.one_rev=1;

plot_dist(handles);
methods={'Overlapped Sine Sweep (log)','Sine Sweep (log)','MLS','Signal_x'};
load('microphone_list');

% to restore or manually edit default microphone list uncomment following once: 

% microphone_list={'SP-EBM-1','SP-XLRM-MINI-2','MS-TFB-2-80049','..p�idat mikrofon'};
% save('microphone_list','microphone_list');

set(handles.method_pop,'String',methods);
set(handles.mics,'String',microphone_list);





% UIWAIT makes HRTFmeas wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = HRTFmeas_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in turn_test.
function turn_test_Callback(hObject, eventdata, handles)
% hObject    handle to turn_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
if info.connected==1
    test_turn=str2num(get(handles.turn_test1,'String'));
    for i=1:length(test_turn)
        turn(test_turn(i),handles);
    end
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end




function turn_test1_Callback(hObject, eventdata, handles)
% hObject    handle to turn_test1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of turn_test1 as text
%        str2double(get(hObject,'String')) returns contents of turn_test1 as a double


% --- Executes during object creation, after setting all properties.
function turn_test1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to turn_test1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in back_to_zero.
function back_to_zero_Callback(hObject, eventdata, handles)
% hObject    handle to back_to_zero (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
if info.connected==1
    turn(-info.pozice,handles);
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end



% --- Executes on slider movement.
function speed_Callback(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
send_speed(handles);


function send_speed(handles)
global ard;
result = round(get(handles.speed,'Value')/0.2)*0.2;
set(handles.speed_text1,'Value',result);
set(handles.speed_text1,'String',num2str(result));
x=1;
while x<11
    if(result==0.2*x)
        fwrite(ard,250-x);
        x=11;
    end
    x=x+1;
end

% --- Executes during object creation, after setting all properties.
function speed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function ard_port_Callback(hObject, eventdata, handles)
% hObject    handle to ard_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ard_port as text
%        str2double(get(hObject,'String')) returns contents of ard_port as a double


% --- Executes during object creation, after setting all properties.
function ard_port_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ard_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ard_connect.
function ard_connect_Callback(hObject, eventdata, handles)
% hObject    handle to ard_connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global info;
global ard;
if info.connected==0
    delete(instrfind({'Port'},{get(handles.ard_port, 'String')})) % pro jistotu - p�i neodpojen� z�stane viset v pam�ti
    ard=serial(get(handles.ard_port, 'String'),'BaudRate',115200);  %zaveden� Arduina jako s�riov� objekt
    if exist('ard','var') && isvalid(ard),
        fopen(ard); %otev�en� pro z�pis
        pause(2);
        fwrite(ard,255); %pos�l�n� ��sla 255 = inicializace
        info.connected=1;
        set(handles.ard_status, 'String','p�ipojeno')
        set(handles.ard_status, 'ForegroundColor','green')
        set(handles.text_info,'String','Arduino bylo �spe�n� p�ipojeno.')
        set(handles.ard_connect, 'String', 'Odpojit');
    else
        set(handles.text_info,'String','Spojen� nebylo nav�z�no.')
    end
else
    delete(instrfind({'Port'},{get(handles.ard_port, 'String')}))
    set(handles.ard_status, 'String','odpojeno')
    set(handles.ard_status, 'ForegroundColor','red')
    set(handles.text_info,'String','Arduino bylo �spe�n� odpojeno.')
    set(handles.ard_connect, 'String', 'P�ipojit');
    pause(0.5);
    info.connected=0;
end


% --- Executes on button press in testrun.
function testrun_Callback(hObject, eventdata, handles)
% hObject    handle to testrun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of testrun

function turn(angle, handles)
send_speed(handles);
global ard;
global info;
% if get(handles.testrun,'Value')==0 %pro testovac� motor je jin� ��zen�
    steps=angle*30;
% else
%     steps=round((angle/1.8)*8); %testovac� motor
% end
info.pozice=info.pozice+angle; %evidence pozice
if steps<0 %zm�na sm�ru ot��en�
    steps=abs(steps);
    fwrite(ard,250);
else
    fwrite(ard,251);
end
pause(0.2);
if(get(handles.fade_run,'Value')==0) %pokud nen� zvolen postupn� rozb�h/dob�h
    while(steps>0) %cyklus ot��en� - d�len� celkov�ho po�tu instrukc� po max. 230
       if(steps>230)
           fwrite(ard,230);
           steps=steps-230;
       else
           fwrite(ard,steps);           
           steps=0;
       end       
    end
    doba=(((11-(get(handles.speed,'Value')/0.2))*200)*2*abs(angle)*30)/1000000;
    disp(['Estimated time is ' num2str(doba) ' seconds.'])
    pause(doba);
else
    
%     fwrite(ard,252); steps=step-20; %rozb�h
%     while(steps>20)
%        if(steps>230)
%            fwrite(ard,230);
%            steps=steps-230;
%        else
%            fwrite(ard,steps);
%            steps=0;
%        end       
%     end
%     fwrite(ard,253); steps=0; %dob�h
end


% --- Executes on button press in fade_run.
function fade_run_Callback(hObject, eventdata, handles)
% hObject    handle to fade_run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fade_run


% --- Executes on button press in layout.
function layout_Callback(hObject, eventdata, handles)
% hObject    handle to layout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in method_pop.
function method_pop_Callback(hObject, eventdata, handles)
% hObject    handle to method_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method_pop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method_pop

% vyber metody 

metoda=get(handles.method_pop,'String');
popupvalue = metoda{get(handles.method_pop,'Value')};
switch(popupvalue)
    case 'Sine Sweep (log)'
%         set(handles.text19,'String','D�lka [s] =');
        set(handles.text19,'String','D�lka sekvence (2^N)/fs, N =');
%         set(handles.seq_length,'String',num2str(2));
        set(handles.seq_length,'String',num2str(14));
        set(handles.seq_pre,'Visible','Off');
        set(handles.text2box,'Visible','Off');
        set(handles.seq_count,'Visible','Off');
        set(handles.text3box,'Visible','Off');
        set(handles.text4box,'Visible','Off');
        set(handles.box3,'Visible','Off');
        set(handles.box4,'Visible','Off');
    case 'MLS'
        set(handles.text19,'String','D�lka sekvence 2^(N-1), N =');
        set(handles.seq_length,'String',num2str(14));
        set(handles.seq_pre,'Visible','On');
        set(handles.seq_pre,'Value',1);
        set(handles.text2box,'Visible','On');
        set(handles.text2box,'String','Po�et sekvenc�');
        set(handles.seq_count,'Visible','On');
        set(handles.seq_count,'String','3');
        set(handles.text3box,'Visible','Off');
        set(handles.text4box,'Visible','Off');
        set(handles.box3,'Visible','Off');
        set(handles.box4,'Visible','Off');
    case 'Overlapped Sine Sweep (log)'
        set(handles.text19,'String','D�lka sekvence (2^N)/fs, N =');
        set(handles.seq_pre,'Visible','Off');
        set(handles.text2box,'Visible','On');
        set(handles.text2box,'String','Doba trv�n� vlastn� IR [s]');
        set(handles.text3box,'Visible','On');
        set(handles.text3box,'String','Doba trv�n� IR druh� harm. [s]');
        set(handles.text4box,'Visible','On');
        set(handles.text4box,'String','Maxim�ln� po�et harm. IR');
        set(handles.seq_count,'Visible','On');
        set(handles.seq_count,'String','0.3');
        set(handles.box3,'Visible','On');
        set(handles.box4,'Visible','On');
end


% --- Executes during object creation, after setting all properties.
function method_pop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on selection change in dist_pop.
function dist_pop_Callback(hObject, eventdata, handles)
% hObject    handle to dist_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dist_pop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dist_pop


% --- Executes during object creation, after setting all properties.
function dist_pop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dist_pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation1_Callback(hObject, eventdata, handles)
% hObject    handle to elevation1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation1 as text
%        str2double(get(hObject,'String')) returns contents of elevation1 as a double


% --- Executes during object creation, after setting all properties.
function elevation1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth1_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth1 as text
%        str2double(get(hObject,'String')) returns contents of azimuth1 as a double


% --- Executes during object creation, after setting all properties.
function azimuth1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation2_Callback(hObject, eventdata, handles)
% hObject    handle to elevation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation2 as text
%        str2double(get(hObject,'String')) returns contents of elevation2 as a double


% --- Executes during object creation, after setting all properties.
function elevation2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth2_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth2 as text
%        str2double(get(hObject,'String')) returns contents of azimuth2 as a double


% --- Executes during object creation, after setting all properties.
function azimuth2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation3_Callback(hObject, eventdata, handles)
% hObject    handle to elevation3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation3 as text
%        str2double(get(hObject,'String')) returns contents of elevation3 as a double


% --- Executes during object creation, after setting all properties.
function elevation3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth3_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth3 as text
%        str2double(get(hObject,'String')) returns contents of azimuth3 as a double


% --- Executes during object creation, after setting all properties.
function azimuth3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation4_Callback(hObject, eventdata, handles)
% hObject    handle to elevation4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation4 as text
%        str2double(get(hObject,'String')) returns contents of elevation4 as a double


% --- Executes during object creation, after setting all properties.
function elevation4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth4_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth4 as text
%        str2double(get(hObject,'String')) returns contents of azimuth4 as a double


% --- Executes during object creation, after setting all properties.
function azimuth4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check1.
function check1_Callback(hObject, eventdata, handles)
% hObject    handle to check1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check1
global info;
info.elevace(1)=get(handles.check1,'Value');
refresh_outputCh(handles);

function refresh_outputCh(handles)
if get(handles.field,'Value')==1
    global info;
    info.elevace(1)=get(handles.check1,'Value');
    info.elevace(2)=get(handles.check2,'Value');
    info.elevace(3)=get(handles.check3,'Value');
    info.elevace(4)=get(handles.check4,'Value');
    info.elevace(5)=get(handles.check5,'Value');
    info.elevace(6)=get(handles.check6,'Value');
    a=[1 2 3 4 5 6];
    a=a.*info.elevace;
    indices=find(a==0);
    a(indices)=[];
    S=num2str(a,'%g,');
    set(handles.output_channels,'String',S(1:end-1));
else
    set(handles.output_channels,'String',num2str(1));
end




function elevation5_Callback(hObject, eventdata, handles)
% hObject    handle to elevation5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation5 as text
%        str2double(get(hObject,'String')) returns contents of elevation5 as a double


% --- Executes during object creation, after setting all properties.
function elevation5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth5_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth5 as text
%        str2double(get(hObject,'String')) returns contents of azimuth5 as a double


% --- Executes during object creation, after setting all properties.
function azimuth5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elevation6_Callback(hObject, eventdata, handles)
% hObject    handle to elevation6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elevation6 as text
%        str2double(get(hObject,'String')) returns contents of elevation6 as a double


% --- Executes during object creation, after setting all properties.
function elevation6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elevation6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function azimuth6_Callback(hObject, eventdata, handles)
% hObject    handle to azimuth6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azimuth6 as text
%        str2double(get(hObject,'String')) returns contents of azimuth6 as a double


% --- Executes during object creation, after setting all properties.
function azimuth6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azimuth6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in check2.
function check2_Callback(hObject, eventdata, handles)
% hObject    handle to check2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check2
global info;
info.elevace(2)=get(handles.check2,'Value');
refresh_outputCh(handles);


% --- Executes on button press in check3.
function check3_Callback(hObject, eventdata, handles)
% hObject    handle to check3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check3
global info;
info.elevace(3)=get(handles.check3,'Value');
refresh_outputCh(handles);


% --- Executes on button press in check4.
function check4_Callback(hObject, eventdata, handles)
% hObject    handle to check4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check4
global info;
info.elevace(4)=get(handles.check4,'Value');
refresh_outputCh(handles);


% --- Executes on button press in check5.
function check5_Callback(hObject, eventdata, handles)
% hObject    handle to check5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check5
global info;
info.elevace(5)=get(handles.check5,'Value');
refresh_outputCh(handles);


% --- Executes on button press in check6.
function check6_Callback(hObject, eventdata, handles)
% hObject    handle to check6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of check6
global info;
info.elevace(6)=get(handles.check6,'Value');
refresh_outputCh(handles);

function plot_dist(handles)

% vykresleni merici site

axes(handles.layout1);
polar(0,0);hold on;view([85 20])
rem_lab = findall(gca,'type','text');
legit = {'0','30','60','90','120','150','180','210','240','270','300','330','360',''};
idx = ~ismember(get(rem_lab,'string'),legit);
set(rem_lab(idx),'string','')
azimut=[];elevace=[];
if(get(handles.check1,'Value')==1)
azimut=str2num(get(handles.azimuth1,'String'));
i=length(azimut);
elevace=90-ones(1,i)*str2double(get(handles.elevation1,'String'));
end
if(get(handles.check2,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth2,'String'))];
i=length(str2num(get(handles.azimuth2,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation2,'String'))];
end
if(get(handles.check3,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth3,'String'))];
i=length(str2num(get(handles.azimuth3,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation3,'String'))];
end
if(get(handles.check4,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth4,'String'))];
i=length(str2num(get(handles.azimuth4,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation4,'String'))];
end
if(get(handles.check5,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth5,'String'))];
i=length(str2num(get(handles.azimuth5,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation5,'String'))];
end
if(get(handles.check6,'Value')==1)
azimut=[azimut str2num(get(handles.azimuth6,'String'))];
i=length(str2num(get(handles.azimuth6,'String')));
elevace=[elevace 90-ones(1,i)*str2double(get(handles.elevation6,'String'))];
end
x=sin(deg2rad(elevace)).*cos(deg2rad(azimut));
y=sin(deg2rad(elevace)).*sin(deg2rad(azimut));
for i=1:length(elevace)
    if elevace(i)<0
        z(i)=-cos(deg2rad(elevace(i)));
    else
        z(i)=cos(deg2rad(elevace(i)));
    end

end
h1=scatter3(x,y,z,25,'filled'); hold on; 
scatter3(0,0,0,500,'filled')
hold off
refresh_outputCh(handles);

% --- Executes on button press in save_dist.
function save_dist_Callback(hObject, eventdata, handles)
% hObject    handle to save_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ulozeni  merici site

if(get(handles.check1,'Value')==1)
    len=length(str2num(get(handles.azimuth1,'String')));
    dist_matrix(1,:)=[str2num(get(handles.elevation1,'String')) str2num(get(handles.azimuth1,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(1,:)=[555 zeros(1,360-1)];
end
if(get(handles.check2,'Value')==1)    
    len=length(str2num(get(handles.azimuth2,'String')));
    dist_matrix(2,:)=[str2num(get(handles.elevation2,'String')) str2num(get(handles.azimuth2,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(2,:)=[555 zeros(1,360-1)];
end
if(get(handles.check3,'Value')==1) 
    len=length(str2num(get(handles.azimuth3,'String')));
    dist_matrix(3,:)=[str2num(get(handles.elevation3,'String')) str2num(get(handles.azimuth3,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(3,:)=[555 zeros(1,360-1)];
end
if(get(handles.check4,'Value')==1) 
    len=length(str2num(get(handles.azimuth4,'String')));
    dist_matrix(4,:)=[str2num(get(handles.elevation4,'String')) str2num(get(handles.azimuth4,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(4,:)=[555 zeros(1,360-1)];
end
if(get(handles.check5,'Value')==1) 
    len=length(str2num(get(handles.azimuth5,'String')));
    dist_matrix(5,:)=[str2num(get(handles.elevation5,'String')) str2num(get(handles.azimuth5,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(5,:)=[555 zeros(1,360-1)];
end
if(get(handles.check6,'Value')==1) 
    len=length(str2num(get(handles.azimuth6,'String')));
    dist_matrix(6,:)=[str2num(get(handles.elevation6,'String')) str2num(get(handles.azimuth6,'String')) 555 zeros(1,360-len-2)];
else
    dist_matrix(6,:)=[555 zeros(1,360-1)];
end
[nazev, path] = uiputfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
varname = genvarname(nazev1);
eval([varname '=dist_matrix;']);
save(full_path,nazev1);


% --- Executes on button press in plot_it.
function plot_it_Callback(hObject, eventdata, handles)
% hObject    handle to plot_it (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
plot_dist(handles)


% --- Executes on button press in load_dist.
function load_dist_Callback(hObject, eventdata, handles)
% hObject    handle to load_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% nacteni merici site

[nazev, path] = uigetfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
load(full_path);
varname= genvarname(nazev1);
eval(['dist_matrix=' varname ';']);
clear azim
if (dist_matrix(1,1)~=555) %v pripade hodnoty 555 v prvnim poli je rovina prazdna
    set(handles.check1,'Value',1); %zaskrtnuti horizontalni roviny
    set(handles.elevation1,'String',num2str(dist_matrix(1,1))); %vyplneni hodnoty elevace
    for i=2:360 %prochazi bunky ulozeneho rozlozeni - ukonceno hodnotou 555
        if dist_matrix(1,i)~=555
            azim(i-1)=dist_matrix(1,i); %cteni azimutu
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' '); %vyplnovani azimutu
    set(handles.azimuth1,'String',no_add_spaces); 
else
    set(handles.check1,'Value',0);
end
clear azim
if (dist_matrix(2,1)~=555)
    set(handles.check2,'Value',1);
    set(handles.elevation2,'String',num2str(dist_matrix(2,1)));
    for i=2:360
        if dist_matrix(2,i)~=555
            azim(i-1)=dist_matrix(2,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth2,'String',no_add_spaces);
else
    set(handles.check2,'Value',0); 
end
clear azim
if (dist_matrix(3,1)~=555)
    set(handles.check3,'Value',1);
    set(handles.elevation3,'String',num2str(dist_matrix(3,1)));
    for i=2:360
        if dist_matrix(3,i)~=555
            azim(i-1)=dist_matrix(3,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth3,'String',no_add_spaces);
else
    set(handles.check3,'Value',0);  
end
clear azim
if (dist_matrix(4,1)~=555)
    set(handles.check4,'Value',1);
    set(handles.elevation4,'String',num2str(dist_matrix(4,1)));
    for i=2:360
        if dist_matrix(4,i)~=555
            azim(i-1)=dist_matrix(4,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth4,'String',no_add_spaces);
else
    set(handles.check4,'Value',0); 
end
clear azim
if (dist_matrix(5,1)~=555)
    set(handles.check5,'Value',1);
    set(handles.elevation5,'String',num2str(dist_matrix(5,1)));
    for i=2:360
        if dist_matrix(5,i)~=555
            azim(i-1)=dist_matrix(5,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth5,'String',no_add_spaces); 
else
    set(handles.check5,'Value',0); 
end
clear azim
if (dist_matrix(6,1)~=555)
    set(handles.check6,'Value',1);
    set(handles.elevation6,'String',num2str(dist_matrix(6,1)));
    for i=2:360
        if dist_matrix(6,i)~=555
            azim(i-1)=dist_matrix(6,i);
        else
            stop=i;
            i=361;            
        end
    end
    no_add_spaces = regexprep(num2str(azim(1:stop-2)),' +',' ');
    set(handles.azimuth6,'String',no_add_spaces); 
else
    set(handles.check6,'Value',0);
end
plot_dist(handles)


% --- Executes during object creation, after setting all properties.
function sync_channel_in_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sync_channel_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function sync_channel_out_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sync_channel_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fs_Callback(hObject, eventdata, handles)
% hObject    handle to fs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fs as text
%        str2double(get(hObject,'String')) returns contents of fs as a double

% pri zmene vzorkovacky se prepocitava vzdalenost odrazu

% refl=str2double(get(handles.reflection_dist,'String'));
% fs=str2double(get(handles.fs,'String'));
% vzorky=floor((1/340)*refl*fs);
% set(handles.window_length,'String',num2str(vzorky));


% --- Executes during object creation, after setting all properties.
function fs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in input_channels.
function input_channels_Callback(hObject, eventdata, handles)
% hObject    handle to input_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function input_channels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function output_channels_Callback(hObject, eventdata, handles)
% hObject    handle to output_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_channels as text
%        str2double(get(hObject,'String')) returns contents of output_channels as a double


% --- Executes during object creation, after setting all properties.
function output_channels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_channels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% mereni 

global info;
global raw_data;
clear raw_data;
metoda=get(handles.method_pop,'String');
popupvalue = metoda{get(handles.method_pop,'Value')};
fs=str2double(get(handles.fs,'String'));
if strcmp('Overlapped Sine Sweep (log)',popupvalue)==1 % v p��pad� OL SS
    method_tag='OL_SS';
    playDeviceName = 'default';
    recDeviceName = 'default';
    inputCh = str2double(strsplit(get(handles.input_channels,'String'),','));
    bufferSize = 2048;
    % repeats=6-length(find(info.elevace==0));
    if get(handles.delay_start,'Value')==1
        for waiting=0:str2double(get(handles.delay_time,'String')); 
            set(handles.text_info,'String',num2str(str2double(get(handles.delay_time,'String'))-waiting));
            pause(1);
        end
    end
    az=[];
    if get(handles.CW,'Value')==0
        no_azimut=0;
        start=0;
    else
        no_azimut=1; %v pripade rotace proti smeru bude otocena hned prvni sekvence rotac�
        start=360;
    end
    e_all=[];
    for i_ele=1:6
        if eval(strcat('get(handles.check',num2str(i_ele),',''Value'')==1'))
            eval(strcat('e_',num2str(i_ele),'=str2num(get(handles.azimuth',num2str(i_ele),',''String''));'));
            eval(strcat('e_all=[e_all, e_',num2str(i_ele),'];'));
        else
            eval(strcat('e_',num2str(i_ele),'=[];'));
        end
    end
    e_all=unique(e_all);
    if start==360
        e_all_edit=fliplr(-(e_all-360));
    else
        e_all_edit=e_all;
    end
    azimut=0;
    for i_az=1:length(e_all)
        pohyb=e_all_edit(i_az)-azimut;
        azimut=e_all_edit(i_az);
        pohyb;
        tic
        if start==360
            turn(-pohyb,handles);
        else
            turn(pohyb,handles);
        end
        toc
        pause(1)
        channels=[];
        curr_eles=[];
        for i_ele=1:6
            eval(strcat('to_compare=e_',num2str(i_ele),';'));
            if start==360
                index=length(e_all)+1-i_az;
            else
                index=i_az;
            end
            if any(e_all(index)==to_compare);
                channels=[channels i_ele];
                eval(strcat('curr_ele=get(handles.elevation',num2str(i_ele),',''String'')'))
                curr_eles=[curr_eles, '_', num2str(mod(str2double(curr_ele),360))];
            end
        end
        curr_eles=curr_eles(2:end); %mazani prvniho podtrzitka
%         eval(strcat('curr_ele=get(handles.elevation',num2str(i_ele),',''String'')'))
        set(handles.text_info,'String',horzcat('Aktualni merene elevace: ',curr_eles,'. Aktualni mereny azimut: ', num2str(e_all(index)),'.'));
        pause(0.001)
        
        [sig_mat, ~, ~, delta_T_OL_sam]=generate_SS_OL_matrix(fs,str2double(get(handles.seq_count,'String')),str2double(get(handles.box3,'String')),str2double(get(handles.box4,'String')),100,20e3,str2double(get(handles.seq_length,'String')),length(channels),0.01); %generovani signalu
           

        recData = PaR1(sig_mat, sig_mat(:,1) ,playDeviceName,channels,recDeviceName,inputCh,bufferSize, fs);
        eval(strcat('raw_data.e',curr_eles,'.a',num2str(e_all(index)),'.left=recData(:,1);'));
        eval(strcat('raw_data.e',curr_eles,'.a',num2str(e_all(index)),'.right=recData(:,2);'));
            
    end
    if start==360
        turn(azimut,handles);
    else
        turn(-azimut,handles);
    end
else
    switch(popupvalue)
        case 'Sine Sweep (log)'
    %         sig=generate_sine_sweep(fs,str2double(get(handles.seq_length,'String')),100,22000,str2double(get(handles.seq_length,'String'))*0.05,str2double(get(handles.seq_count,'String')));
            sig = 0.9*generate_sinesweeps(100,20000,fs,str2double(get(handles.seq_length,'String')));
            method_tag='SweepSine';
%             size(sig)
        case 'MLS'
            sig=0.9*GenerateMLSSequence(str2double(get(handles.seq_count,'String')),str2double(get(handles.seq_length,'String')),0)';
            method_tag='MLS';
%             size(sig)
%         case 'MESM'
%             warndlg('Metoda nen� implementov�na','Metoda nen� implementov�na');
%             method_tag='MESM';        
        case 'Signal_x'
            method_tag='Signal_x';
    %         load('signal_x.mat');   
            sig=audioread('test_signal.aif');
            sig=sig(:,1)';
            size(sig)
    end
    delta_T_OL_sam=0; %hodnota v metadatech v p��pad� OL SS
    meaSig  = sig';
    sync = meaSig;
    playDeviceName = 'default';
    outputCh = str2double(strsplit(get(handles.output_channels,'String'),','));
    recDeviceName = 'default';
    inputCh = str2double(strsplit(get(handles.input_channels,'String'),','));
    bufferSize = 2048;
    % repeats=6-length(find(info.elevace==0));
    if get(handles.delay_start,'Value')==1
        for waiting=0:str2double(get(handles.delay_time,'String')); 
            set(handles.text_info,'String',num2str(str2double(get(handles.delay_time,'String'))-waiting));
            pause(1);
        end
    end
    az=[];
    if get(handles.CW,'Value')==0
        no_azimut=0;
        start=0;
    else
        no_azimut=1; %v pripade rotace proti smeru bude otocena hned prvni sekvence rotac�
        start=360;
    end
    if info.one_rev==0
        for i_ele=1:6
            if eval(strcat('get(handles.check',num2str(i_ele),',''Value'')==1'))
                no_azimut=no_azimut+1;
                eval(strcat('az1=str2num(get(handles.azimuth',num2str(i_ele),',''String''));'));
                az1=sort(az1);
                if mod(no_azimut,2)==0
                    az=[az fliplr(az1)];
                else
                    az=[az az1];
                end

            end
        end
        points=length(az);
        az(1,points+1)=start; %na konci zas na po��te�n� pozici // az je vektor azimut�
        pohyb=zeros(points+1,1); %pohyb je vektor rotac�
        for ii=1:points+1
            if ii==1
                pohyb(ii)=az(ii)-start;
            else
               pohyb(ii)=az(ii)-az(ii-1);
            end
        end
        start_point=0;
        no_azimut=0; %po��tadlo "aktivn�ch" elevac�
        for i=1:6
            if eval(strcat('get(handles.check',num2str(i),',''Value'')==1'))
                no_azimut=no_azimut+1;
                eval(strcat('elevace=str2num(get(handles.elevation',num2str(i),',''String''));'));
                if get(handles.field,'Value')==0 %pokud je jeden rep. zobraz� se hl�ka ke zm�n� elevace
                    show_dlg_pozice(num2str(elevace));
                end
                eval(strcat('azimuty=str2num(get(handles.azimuth',num2str(i),',''String''));'));
                for ii=start_point+1:start_point+1+length(azimuty)-1
                    set(handles.text_info,'String',horzcat('Aktualni merena elevace: ', num2str(elevace),'. Aktualni mereny azimut: ', num2str(az(ii)),'.'));
                    tic
                    turn(pohyb(ii),handles);
                    toc
                    if get(handles.field,'Value')==0 %pokud je jeden rep. z�st�v� channel dle GUI
                        recData = PaR(meaSig, sync ,playDeviceName,outputCh,recDeviceName,inputCh,bufferSize, fs);
                        else
                        recData = PaR(meaSig, sync ,playDeviceName,outputCh(no_azimut),recDeviceName,inputCh,bufferSize, fs);
                    end
                    eval(strcat('raw_data.e',num2str(mod(elevace,360)),'.a',num2str(az(ii)),'.left=recData(:,1);'));
                    eval(strcat('raw_data.e',num2str(mod(elevace,360)),'.a',num2str(az(ii)),'.right=recData(:,2);'));
                end
                start_point=ii;
            end
        end    
        turn(pohyb(end),handles); %posledni rotace (nazpet)
        if info.pozice~=0
            turn(-info.pozice,handles);
        end
    else
        e_all=[];
        for i_ele=1:6
            if eval(strcat('get(handles.check',num2str(i_ele),',''Value'')==1'))
                eval(strcat('e_',num2str(i_ele),'=str2num(get(handles.azimuth',num2str(i_ele),',''String''));'));
                eval(strcat('e_all=[e_all, e_',num2str(i_ele),'];'));
            else
                eval(strcat('e_',num2str(i_ele),'=[];'));
            end
        end
        e_all=unique(e_all);
        if start==360
            e_all_edit=fliplr(-(e_all-360));
    %         e_all_edit(e_all_edit==360)=0;
    %         e_all_edit=sort(e_all_edit);
        else
            e_all_edit=e_all;
        end
        azimut=0;
        e_all_edit
        e_all
        for i_az=1:length(e_all)
            pohyb=e_all_edit(i_az)-azimut;
            azimut=e_all_edit(i_az);
            pohyb
            tic
            if start==360
                turn(-pohyb,handles);
            else
                turn(pohyb,handles);
            end
            toc
            pause(1)
            for i_ele=1:6
                eval(strcat('to_compare=e_',num2str(i_ele),';'));
                if start==360
                    index=length(e_all)+1-i_az;
                else
                    index=i_az;
                end
                if any(e_all(index)==to_compare);
                    eval(strcat('curr_ele=get(handles.elevation',num2str(i_ele),',''String'')'))
                    set(handles.text_info,'String',horzcat('Aktualni merena elevace: ',curr_ele,'. Aktualni mereny azimut: ', num2str(e_all(index)),'.'));
                    pause(0.001)
                    recData = PaR(meaSig, sync ,playDeviceName,i_ele,recDeviceName,inputCh,bufferSize, fs);
                    eval(strcat('raw_data.e',num2str(mod(str2double(curr_ele),360)),'.a',num2str(e_all(index)),'.left=recData(:,1);'));
                    eval(strcat('raw_data.e',num2str(mod(str2double(curr_ele),360)),'.a',num2str(e_all(index)),'.right=recData(:,2);'));
                end
            end
        end
        if start==360
            turn(azimut,handles);
        else
            turn(-azimut,handles);
        end
    end    
end
%%
seznam_mics=get(handles.mics,'String');
seznam_gen=get(handles.Gender,'String');

raw_data.metadata={'method' 'sampling_frequency' 'sequence_length' 'sequence_count' 'presends' 'microphones' 'firstname' 'lastname' 'gender' 'age' 'date' 'delta_T_OL (samples)';method_tag num2str(fs) get(handles.seq_length,'String') get(handles.seq_count,'String') num2str(get(handles.seq_pre,'Value')) seznam_mics(get(handles.mics,'Value')) get(handles.FirstName,'String') get(handles.LastName,'String') char(seznam_gen(get(handles.Gender,'Value'))) get(handles.Age,'String') date num2str(delta_T_OL_sam)};
%test% eval(strcat('raw_data.e',num2str(elevace),'.a',num2str(azimuty(ii)),'.left=sig;')); %zmenit sig!!
%test% eval(strcat('raw_data.e',num2str(elevace),'.a',num2str(azimuty(ii)),'.right=sig;')); %zmenit sig!!
raw_file_name=strcat(get(handles.measure_tag,'String'),'_raw_data_',method_tag);
eval(strcat('save(''namerena_data\',raw_file_name,''',''raw_data'');'));
eval(strcat('set(handles.text_info,''String'',''M��en� dokon�eno a syrov� data zaps�na do souboru: ',raw_file_name,'.mat'');'));



% figure(1)
% plot(sig)

function show_dlg_pozice(hodnota)

% dialog s pokynem

    d = dialog('Position',[300 300 250 150],'Name','Instrukce');

    txt = uicontrol('Parent',d,...
               'Style','text',...
               'Position',[20 80 210 60],...
               'String',horzcat('Nastavte reproduktor do �hlu ', hodnota, '�. Po celou dobu m��en� udr�ujte konstatn� vz�dlenost od hlavy. Po stisknut� OK za�ne m��ic� sekvence.'));

    btn = uicontrol('Parent',d,...
               'Position',[85 20 70 25],...
               'String','OK',...
               'Callback','delete(gcf)');
    uiwait(d);




% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in sound_test.
function sound_test_Callback(hObject, eventdata, handles)
% hObject    handle to sound_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sig = 0.9*generate_sinesweeps(100,20000,str2double(get(handles.fs,'String')),15);
meaSig  = sig';
sync = meaSig;
playDeviceName = 'default';
% outputCh = str2double(strsplit(get(handles.output_channels,'String'),','));
outputCh = 4;
recDeviceName = 'default';
inputCh = str2double(strsplit(get(handles.input_channels,'String'),','));
bufferSize = 2048;
pause(0.1)

[recData] = PaR(meaSig,sync,playDeviceName,outputCh,recDeviceName,inputCh,bufferSize,str2double(get(handles.fs,'String')));
%testPaR();

figure;
subplot(211);
plot(recData(:,1));
subplot(212);
plot(recData(:,2));



function seq_length_Callback(hObject, eventdata, handles)
% hObject    handle to seq_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of seq_length as text
%        str2double(get(hObject,'String')) returns contents of seq_length as a double


% --- Executes during object creation, after setting all properties.
function seq_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seq_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function seq_count_Callback(hObject, eventdata, handles)
% hObject    handle to seq_count (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of seq_count as text
%        str2double(get(hObject,'String')) returns contents of seq_count as a double


% --- Executes during object creation, after setting all properties.
function seq_count_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seq_count (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in seq_pre.
function seq_pre_Callback(hObject, eventdata, handles)
% hObject    handle to seq_pre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of seq_pre


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in meas_ir.
function meas_ir_Callback(hObject, eventdata, handles)
% hObject    handle to meas_ir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in eq.
function eq_Callback(hObject, eventdata, handles)
% hObject    handle to eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of eq



function window_length_Callback(hObject, eventdata, handles)
% hObject    handle to window_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of window_length as text
%        str2double(get(hObject,'String')) returns contents of window_length as a double
vzorky=str2double(get(handles.window_length,'String'));
fs=str2double(get(handles.fs,'String'));
refl=round(vzorky*340/fs,2);
set(handles.reflection_dist,'String',num2str(refl));


% --- Executes during object creation, after setting all properties.
function window_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to window_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function reflection_dist_Callback(hObject, eventdata, handles)
% hObject    handle to reflection_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of reflection_dist as text
%        str2double(get(hObject,'String')) returns contents of reflection_dist as a double
refl=str2double(get(handles.reflection_dist,'String'));
fs=str2double(get(handles.fs,'String'));
vzorky=floor((1/340)*refl*fs);
set(handles.window_length,'String',num2str(vzorky));


% --- Executes during object creation, after setting all properties.
function reflection_dist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to reflection_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ir_cut.
function ir_cut_Callback(hObject, eventdata, handles)
% hObject    handle to ir_cut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ir_cut


% --- Executes on button press in motor_ena.
function motor_ena_Callback(hObject, eventdata, handles)
% hObject    handle to motor_ena (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of motor_ena
global ard;
global info;
if info.connected==1
    if(get(handles.motor_ena,'Value')==1)
        fwrite(ard,252);
    else
        fwrite(ard,253);    
    end
else
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
end


% --- Executes on button press in CW.
function CW_Callback(hObject, eventdata, handles)
% hObject    handle to CW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CW
global info
if info.connected~=1
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
    if get(handles.CCW,'Value')==1
        set(handles.CCW,'Value',0);
        set(handles.CW,'Value',1);
    else
        set(handles.CCW,'Value',1);
        set(handles.CW,'Value',0);
    end
else
    set(handles.turn_test1,'String','-360;360');
end


% --- Executes on button press in CCW.
function CCW_Callback(hObject, eventdata, handles)
% hObject    handle to CCW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CCW
global info
if info.connected~=1
    warndlg('P�ipojte pros�m Arduino!','Arduino nen� p�ipojeno')
    if get(handles.CCW,'Value')~=1
        set(handles.CCW,'Value',0);
        set(handles.CW,'Value',1);
    else
        set(handles.CCW,'Value',1);
        set(handles.CW,'Value',0);
    end
else
    set(handles.turn_test1,'String','360;-360');
end


% --- Executes on button press in analyze.
function analyze_Callback(hObject, eventdata, handles)
% hObject    handle to analyze (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function measure_tag_Callback(hObject, eventdata, handles)
% hObject    handle to measure_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of measure_tag as text
%        str2double(get(hObject,'String')) returns contents of measure_tag as a double


% --- Executes during object creation, after setting all properties.
function measure_tag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to measure_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_raw_data.
function load_raw_data_Callback(hObject, eventdata, handles)
% hObject    handle to load_raw_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global raw_data;
[nazev, path] = uigetfile({'*.mat'},'File Selector');
full_path = strcat(path, nazev);
nazev1=nazev(1:end-4);
load(full_path);
set(handles.raw_data_name,'String',nazev);


% --- Executes on button press in manual_window.
function manual_window_Callback(hObject, eventdata, handles)
% hObject    handle to manual_window (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of manual_window


% --- Executes on button press in degrees.
function degrees_Callback(hObject, eventdata, handles)
% hObject    handle to degrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of degrees
if get(handles.degrees,'Value')==1
    set(handles.elevation1,'String',num2str(str2num(get(handles.elevation1,'String'))+10));
    set(handles.elevation2,'String',num2str(str2num(get(handles.elevation2,'String'))+10));
    set(handles.elevation3,'String',num2str(str2num(get(handles.elevation3,'String'))+10));
    set(handles.elevation4,'String',num2str(str2num(get(handles.elevation4,'String'))+10));
    set(handles.elevation5,'String',num2str(str2num(get(handles.elevation5,'String'))+10));
    set(handles.elevation6,'String',num2str(str2num(get(handles.elevation6,'String'))+10));
else
    set(handles.elevation1,'String',num2str(str2num(get(handles.elevation1,'String'))-10));
    set(handles.elevation2,'String',num2str(str2num(get(handles.elevation2,'String'))-10));
    set(handles.elevation3,'String',num2str(str2num(get(handles.elevation3,'String'))-10));
    set(handles.elevation4,'String',num2str(str2num(get(handles.elevation4,'String'))-10));
    set(handles.elevation5,'String',num2str(str2num(get(handles.elevation5,'String'))-10));
    set(handles.elevation6,'String',num2str(str2num(get(handles.elevation6,'String'))-10));
end


% --- Executes when selected object is changed in uipanel4.
function uipanel4_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel4 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
refresh_outputCh(handles);


% --- Executes on button press in checkbox16.
function checkbox16_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox16


% --- Executes on button press in run_analysis.
function run_analysis_Callback(hObject, eventdata, handles)
% hObject    handle to run_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
HRTFmeas_analyze;


% --- Executes on button press in delay_start.
function delay_start_Callback(hObject, eventdata, handles)
% hObject    handle to delay_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of delay_start



function delay_time_Callback(hObject, eventdata, handles)
% hObject    handle to delay_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of delay_time as text
%        str2double(get(hObject,'String')) returns contents of delay_time as a double


% --- Executes during object creation, after setting all properties.
function delay_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to delay_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mics.
function mics_Callback(hObject, eventdata, handles)
% hObject    handle to mics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mics contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mics
load('microphone_list');
if length(microphone_list)==get(handles.mics,'Value')
    newMic=inputdlg('Zadejte n�zev nov�ho mikrofonu:');
    microphone_list{end}=char(newMic);
    microphone_list{end+1}=char('..p�idejte mikrofon');
    save('xxx','microphone_list')
    set(handles.mics,'String',microphone_list);
    set(handles.mics,'Value',length(microphone_list)-1);
    save('microphone_list','microphone_list');
end



% --- Executes during object creation, after setting all properties.
function mics_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in uibuttongroup1.
function uibuttongroup1_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uibuttongroup1 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global info;
if get(handles.one_revolution,'Value')==0
    info.one_rev=0;
else
    info.one_rev=1;
end



function FirstName_Callback(hObject, eventdata, handles)
% hObject    handle to FirstName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FirstName as text
%        str2double(get(hObject,'String')) returns contents of FirstName as a double


% --- Executes during object creation, after setting all properties.
function FirstName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FirstName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LastName_Callback(hObject, eventdata, handles)
% hObject    handle to LastName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LastName as text
%        str2double(get(hObject,'String')) returns contents of LastName as a double


% --- Executes during object creation, after setting all properties.
function LastName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LastName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Age_Callback(hObject, eventdata, handles)
% hObject    handle to Age (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Age as text
%        str2double(get(hObject,'String')) returns contents of Age as a double


% --- Executes during object creation, after setting all properties.
function Age_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Age (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Gender.
function Gender_Callback(hObject, eventdata, handles)
% hObject    handle to Gender (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Gender contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Gender


% --- Executes during object creation, after setting all properties.
function Gender_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Gender (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box3_Callback(hObject, eventdata, handles)
% hObject    handle to box3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box3 as text
%        str2double(get(hObject,'String')) returns contents of box3 as a double


% --- Executes during object creation, after setting all properties.
function box3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function box4_Callback(hObject, eventdata, handles)
% hObject    handle to box4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of box4 as text
%        str2double(get(hObject,'String')) returns contents of box4 as a double


% --- Executes during object creation, after setting all properties.
function box4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to box4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
