function [mls_seq, mls]=generate_mls(n,repetitions) 

switch n								
case 2								
   count=2;							
   bit1=1;							
   bit2=2;							
case 3
   count=2;
   bit1=1;
   bit2=3;
case 4
   count=2;
   bit1=1;
   bit2=4;
case 5
   count=2;
   bit1=2;
   bit2=5;
case 6
   count=2;
   bit1=1;
   bit2=6;
case 7
   count=2;
   bit1=1;
   bit2=7;
case 8
   count=4;
   bit1=2;
   bit2=3;
   bit3=4;
   bit4=8;
case 9
   count=2;
   bit1=4;
   bit2=9;
case 10
   count=2;
   bit1=3;
   bit2=10;
case 11
   count=2;
   bit1=2;
   bit2=11;
case 12
   count=4;
   bit1=1;
   bit2=4;
   bit3=6;
   bit4=12;
case 13
   count=4;
   bit1=1;
   bit2=3;
   bit3=4;
   bit4=13;
case 14
   count=4;
   bit1=1;
   bit2=3;
   bit3=5;
   bit4=14;
case 15
   count=2;
   bit1=1;
   bit2=15;
case 16
   count=4;
   bit1=2;
   bit2=3;
   bit3=5;
   bit4=16;
case 17
   count=2;
   bit1=3;
   bit2=17;
case 18
   count=2;
   bit1=7;
   bit2=18;
case 19
   count=4;
   bit1=1;
   bit2=2;
   bit3=5;
   bit4=19;
case 20
   count=2;
   bit1=3;
   bit2=20;
end


while 1
    seq1 = round(rand(1,n));
    if find(seq1==1)
        break
    end
end

for i = (2^n)-1:-1:1
      
   feedback1 = xor(seq1(bit1),seq1(bit2));
   
   if count==4
      feedback2 = xor(seq1(bit3),seq1(bit4));
      feedback1 = xor(feedback1,feedback2);	
   end
   
	seq1 = [feedback1 seq1(1:n-1)];
	mls(i) = (-2 .* feedback1) + 1;  	%p�epo�et na -1,1 z 0,1
end

mls_seq=[];
for i=1:repetitions
    mls_seq=[mls_seq mls];
end

% subplot(221); plot(mls);
% subplot(222); plot(abs(fft(mls)));
% subplot(2,2,[3 4]); plot(cconv(mls,fliplr(mls),length(mls)));