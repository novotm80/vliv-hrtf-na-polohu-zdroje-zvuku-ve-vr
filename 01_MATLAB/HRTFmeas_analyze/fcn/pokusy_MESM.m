clear all; clc; %close all;

tauIR=1; % doba dozvuku z�kl. IR [s]
tauIR2=0.01; % doba dozvuku 2. IR [s]
kmax=3; % max po�et harmonick�ch
f1=100; f2=20000; %hrani�n� frekvence sweepu [Hz]
tau_SW=[0.5:0.5:50]; % doba trv�n� sweepu [s]
% tau_SW=0.3;
N=6; % po�et reproduktor�
n=2; % po�et syst�mu s IL 

%% ES
tau_ES=N*(tau_SW+tauIR); % doba trv�n� m��en� bez OL a IL
r_s=(log2(f2/f1))./tau_SW; % sweep rate
delta_t_kmax=log2(kmax)./r_s; % vzd�lenost vlastn� IR od posledn� harmonick� [s]
delta_t_2=log2(2)./r_s; % vzd�lenost vlastn� IR od druh� harmonick� [s]

if r_s>(1/tauIR2) % podm�nka pro sweep
    errordlg('Vlastn� IR koliduje s 2. harmonickou. Sni�te rychlost p�ela�ov�n� sweepu!','Sweep error');
end

%% OL
delta_T_OL=delta_t_kmax+tauIR; % minim�ln� zpo�d�n� mezi sweepy
tauOL=tau_SW+(N-1)*(delta_t_kmax+tauIR); % doba trv�n� m��en� p�i pou�it� OL 

disp('OL'); disp(' ');
disp(['Minim�ln� zpo�d�n� mezi sweepy: ' num2str(delta_T_OL) 's']);

%%
figure(1)
plot(tau_SW,tau_ES,'LineWidth',2); hold on;
plot(tau_SW,tauOL,'LineWidth',2);
%% IL + MESM
% n=((tauIR-tauIR2+(1./r_s))/tauIR); %odhad n
for n=[2 3 6]

    tau_blank=delta_t_2-tauIR2; % voln� prostor mezi vlastn� a 2. harmonickou
    tau_remaining_1sts=tauIR*(n-1); % �as pot�ebn� pro vlastn� harmonick� prokl�dan�ch syst�m�
    

    tau_SWo=((n-1)*tauIR+tauIR2)*r_s.*tau_SW; %minim�ln� doba sweepu
    tau_SWo=tau_SW; %pokud nechci vyu��vat minim�ln� dobu sweepu

    tauIL=(N./n).*tau_SWo+N*tauIR;

    disp(' '); disp('IL'); disp(' ');
    disp(['Odhadnut� po�et syst�mu s IL = ' num2str(n)]);

    disp(' ');
    disp(['Doba trv�n� m��en� bez OL a IL: ' num2str(tau_ES) ' s. (' num2str(tau_SW) ')']);
    disp(['Doba trv�n� m��en� s OL: ' num2str(tauOL) ' s. (' num2str(tau_SW) ')']);
    disp(['Doba trv�n� m��en� s IL: ' num2str(tauIL) ' s. (' num2str(tau_SW) ')']);
    
    delta_t_kmaxo=(tau_SWo./(r_s.*tau_SW)).*log2(kmax);
    tauMESM=tau_SWo+delta_t_kmaxo*((N/n)-1)+N*tauIR;
    
    for i=1:length(tau_blank)
        if tau_blank(i)<=tau_remaining_1sts
%             errordlg('Druh� harmonick� prvn�ho syst�mu koliduje s vlastn�mi harmonick�mi ostatn�ch syst�m�. Sni�te po�et syst�m�, nebo upravte parametry sweepu.','Interleaving error');
            tauIL(i)=NaN;
            tauMESM(i)=NaN;
        end
    end

    plot(tau_SW,tauIL,'LineWidth',2);
    plot(tau_SW,tauMESM,'--','LineWidth',2);
end
%%
hold off;
legend('bez IL a OL', 's OL', 's IL(\eta=2)', 's MESM(\eta=2)', 's IL(\eta=3)', 's MESM(\eta=3)','s IL(\eta=6)','s MESM(\eta=6)');
xlabel('doba trvani jednotliveho sweepu [s]')
ylabel('doba trvani cel�ho mereni pro N systemu [s]')
title(horzcat('\tau_{IR} = ',num2str(tauIR),' s, \tau_{IR,2} = ',num2str(tauIR2),' s, N = ', num2str(N),', k_{max} = 3, f_1 = 100 Hz, f_2 = 20 kHz'))

%%

