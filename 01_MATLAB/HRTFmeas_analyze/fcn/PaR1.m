function [recData] = PaR(meaSig, sync, playDeviceName,outputCh,recDeviceName,inputCh,bufferSize, fs, queueDuration )
%simultanious play of file and record the response
%Made by Jaroslav Bouse, CTU in Prague, FEE
if nargin<9
    queueDuration = 0.1;
end
% synchronization channel #   in current implementation same for plaz and record  
synCh = 7;


%system latency - look for the value in the soundcard driver...
sLatency = 2048;
% size(input);

%initialisation of output and input devices
wlen = bufferSize;
N = length(outputCh);

outputCh = [outputCh, synCh];
inputCh = [inputCh, synCh];
%input device
hIn = dsp.AudioRecorder('DeviceName', recDeviceName, 'SampleRate', fs, 'ChannelMappingSource', 'Property', 'ChannelMapping',inputCh, ...
                        'BufferSizeSource', 'Property', 'BufferSize', bufferSize, 'SamplesPerFrame', wlen );
%output device                    
hOut = dsp.AudioPlayer('DeviceName', playDeviceName,'SampleRate', fs, 'ChannelMappingSource', 'Property', 'ChannelMapping',outputCh, ...
                       'QueueDuration',queueDuration , 'BufferSizeSource', 'Property', 'BufferSize', bufferSize);  
                   
                   
%disp('playAndRecord has been Started'); 
recData = [];
latency = 0.1*fs+2*wlen + sLatency;   % compensate input+output latency  - magic...
% add sync channel
% meaSig_orig=meaSig;
% for i=1:length(outputCh)-2
%     meaSig = [meaSig,meaSig_orig];
% end
meaSig = [meaSig, sync];
size(meaSig)
size(zeros(2*latency,length(outputCh)))
meaSig = [meaSig; zeros(2*latency,length(outputCh))];
M =floor(length(meaSig)/wlen);
frame = step(hIn);  
recData = [recData;frame];

for i=1:M+3
    if i<=M
        ii=(i-1)*wlen+1;
        kk = ii+wlen-1;
        output = meaSig(ii:kk,:);
%         size(output)
        step(hOut, output);
    elseif i==M+1
        output = meaSig(kk:end,:);
%         size(output)
        step(hOut, output);
    end;  
    frame = step(hIn);  
    recData = [recData;frame]; 
%    step(hmfw, step(har));  
end

% recData = recData(latency+1:end-1.5*wlen,:);
% 
% 
% %% synchronization between input and output channel
% % size(recData)
% % size(input)
% 
% [acor,lag] = xcorr(recData(:,N+1),sync, 1024);
% [~,I] = max(abs(acor));
%     timeDiff = lag(I)  
% %     figure; plot(recData(:,2))
% recData = recData(1+timeDiff:end,1:N);  
    

% release input&output devices
release(hOut);
release(hIn);
pause(0.5);
%disp('Data was successfuly recorded')













