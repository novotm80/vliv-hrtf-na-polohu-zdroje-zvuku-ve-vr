
            fs = 96e3;

            tx = (0:1/fs:5-1/fs)';
            
            tone = 0.5*sin(2*pi*440*tx); 
            
            % ramping
            ramp_dur = 50e-3; % ms duration of ramp
            x = [0:1/fs:ramp_dur]';
            x = pi*x/ramp_dur;
            rampUp = 0.5*(1 - cos(x));
            rampDown = flipud(rampUp);
            whole_ramp = [rampUp; ones(length(tone)-2*length(x),1); rampDown(1:end)];

            tone = tone.*whole_ramp;
            
            
            meaSig  = [tone];
            sync = tone;
            playDeviceName = 'default';
            outputCh = [1,2];
            recDeviceName = 'default';
            inputCh = [1,2];
            bufferSize = 2048;
            
            


[recData] = PaR(meaSig, sync ,playDeviceName,outputCh,recDeviceName,inputCh,bufferSize, fs);