%%
close all
load('pokusy_ekvalizace2.mat')
raw_hrir = sinesweeps_response(raw_hrir',96000, 14, 100, 20000, 100); 

rawIR=raw_hrir;
rawEQ=eq_l;
%%
% subplot(211);
% plot(abs(fft(rawIR,1024)));
% subplot(212);
% plot(abs(fft(rawEQ,1024)));
filtered_rawIR=rawIR;
filtered_rawEQ=rawEQ;

%% filtrace rawIR
Hd = lowpass_filter(96000,18000,19000);
processed = filter(Hd,rawIR);
Hd = highpass_filter(96000,100,80);
filtered_rawIR = filter(Hd,processed);

%% filtrace rawEQ
Hd = lowpass_filter(96000,18000,19000);
processed = filter(Hd,rawEQ);
Hd = highpass_filter(96000,100,80);
filtered_rawEQ = filter(Hd,processed);
% 
% figure(1); plot(filtered_rawIR);
% figure(2); plot(filtered_rawEQ);

%%

figure(1); plot(filtered_rawIR);
set(gcf,'currentch',char(1)) %reset bufferu s charem
zoom on;
waitfor(gcf,'CurrentCharacter',char(13))% �ek�n� na char - enter !!! ve starsich verzi Matlabu jen ...cter',13)... zde nutno ...cter',char(13))...
zoom reset
zoom off
title('start'); [startIR]=floor(ginput(1));
title('stop'); [stopIR]=ceil(ginput(1));
plot(filtered_rawEQ); 
set(gcf,'currentch',char(1)) %reset bufferu s charem
zoom on;
waitfor(gcf,'CurrentCharacter',char(13))% �ek�n� na char - enter !!! ve starsich verzi Matlabu jen ...cter',13)... zde nutno ...cter',char(13))...
zoom reset
zoom off
title('start'); [startEQ]=floor(ginput(1));
title('stop'); [stopEQ]=ceil(ginput(1));
%% orez IR
cut_rawIR=rawIR(startIR:stopIR);
cut_rawEQ=rawEQ(startEQ:stopEQ);
figure(2)
subplot(211); plot(cut_rawIR);
subplot(212); plot(cut_rawEQ);
%% spektra
spec_cut_rawIR=abs(fft(cut_rawIR,2048));
spec_cut_rawEQ=abs(fft(cut_rawEQ,2048))';
spec_angle_cut_rawIR=angle(fft(cut_rawIR,2048));
spec_angle_cut_rawEQ=angle(fft(cut_rawEQ,2048))';
%% deleni ve spektru, odecitani faze a zpetna fft
spec_abs_equalizedIR=(spec_cut_rawIR./spec_cut_rawEQ);
spec_angle_equalizedIR=spec_angle_cut_rawIR-spec_angle_cut_rawEQ;
spec_equalizedIR=spec_abs_equalizedIR.*exp(1i*spec_angle_equalizedIR);
equalizedIR=real(ifft(spec_equalizedIR));
%% filtrace
Hd = lowpass_filter(96000,18000,19000);
processed = filter(Hd,equalizedIR);
Hd = highpass_filter(96000,100,80);
filtered_equalizedIR = filter(Hd,processed);
figure(3)
subplot(211); plot(filtered_equalizedIR);
subplot(212); fftcko(filtered_equalizedIR,96000);
