%% Script for gathering results data
% Author: Martin Novotny (novotm80@fel.cvut.cz)
close all; clear; clc;

load('test_passed');

%% Load input file and gather data
test_results = cell(1, test_passed); % pre-allocate cell for results)
resp_info = readcell('respondents_data.xlsx');

for id = 1:test_passed
    
    % open selected file
    fname = sprintf('results/%03d.txt', id);
    fid = fopen(fname);
    
    % check, if file was opened successfully
    if fid < 2
        dlg = sprintf('File %03d.txt was not opened successfully', test_passed(id));
        errordlg(dlg, 'MATLAB Error');
        save('test_results_err.mat', 'test_results');
        error('Invalid file identifier!');
    end
    
    test_num = 0; % set serial number of analysed test
    cor_num = 0; % number of correct answers
    line_no = 1; 
   
    % scan lines of text file with results and gather selected data
    while ~feof(fid)
        str = fgetl(fid); % one line of text

        % get info if the answer was correct (C) or incorrect (I)
        if contains(str, '- C')
           test_num = test_num+1;
           res = 1;
           ang = 0;
           cor_num = cor_num+1;
        elseif contains(str, '- I')
           test_num = test_num+1;
           res = 0;
        end

        % get info about time needed for decision
        if contains(str, '- Time')
            time = str2double(extractBefore(extractAfter(str, 'answer: '), ' sec')); % selects the number from structured results text file
            if res
                test_results{id}{test_num+1} = [res time ang];
            end
        end
        
        % get info about the angle error 
        if contains(str, 'Answer coord')
            ang = str2double(extractBefore(extractAfter(str, 'Angle: '), '°'));
            test_results{id}{test_num+1} = [res time ang];
        end
        
        % get info about total test duration
        if contains(str, 'Test duration')
            time_whole_min = str2double(extractBefore(extractAfter(str, 'duration: '), ' min'));
            time_whole_sec = 60*time_whole_min+str2double(extractBefore(extractAfter(str, 'min '), ' sec'));
        end
        
        % get information about respondent
        switch(line_no)
            case 2
                test_results{id}{1}{1} = str; % name
            case 3
                test_results{id}{1}{2} = str; % sensory impairment
            case 4
                test_results{id}{1}{3} = extractAfter(str, 'Interpolation: '); % interpolation
            case 5
                test_results{id}{1}{4} = str; % HRTF type
        end
        
        line_no = line_no+1; % set line number
        
    end
    
        test_results{id}{1}{5} = time_whole_sec; % save whole time needed for response
        test_results{id}{1}{6} = cor_num; % save number of correct responses
        test_results{id}{1}{7} = id; % ID of results file
        
        fclose(fid);
end

%% Pair results from generic and measured HRTF
idx_proc = 0; % indices of processed data
res_id = 1;

% sort results based on name of respondents
for a=1:size(test_results,2)
    if (max(ismember(idx_proc, a))==0)
        name_1 = test_results{a}{1}{1}; % respondent name 1
        type = test_results{a}{1}{4}; % HRTF type (generic/measured)
        
        for b=a+1:size(test_results,2)
            name_2 = test_results{b}{1}{1}; % respondent name 2
            
            if (strcmp(name_1, name_2)) % check if name 1 & name 2 are equal
                 if (strcmp(type, 'Generic HRTF')) % order of sort (GENERIC - MEASURED)
                     sort_val(res_id,1) = a;
                     sort_val(res_id,2) = b;
                 else
                     sort_val(res_id,1) = b;
                     sort_val(res_id,2) = a;
                 end
                 
                 % add processed indices to array
                 res_id = res_id+1;
                 idx_proc(end+1) = a;
                 idx_proc(end+1) = b;
                 
                 % add demographic data about respondents from XLSX file
                 % respondents_data
                 for c = 1:size(resp_info,1)
                   name_resp = resp_info{c,1};

                   if strcmp(name_1, name_resp)
                       test_results{a}{1}{8} = resp_info{c, 2}; % age
                       test_results{a}{1}{9} = resp_info{c, 3}; % sex
                       test_results{a}{1}{10} = resp_info{c, 4}; % VR experience

                       test_results{b}{1}{8} = resp_info{c, 2}; % age
                       test_results{b}{1}{9} = resp_info{c, 3}; % sex
                       test_results{b}{1}{10} = resp_info{c, 4}; % VR experience

                       break;
                   end
            end
        end
        
               
        end
    end
end

% get sorted data - read from selected indices
for a = 1:length(sort_val)
    test_results_sorted{a,1} = test_results{sort_val(a,1)}{1}([1 2 8 9 10]); % data about respondent
    
    test_results_sorted{a,2}{1} = test_results{sort_val(a,1)}{1}([5 6]); % generic HRTF - whole stats
    test_results_sorted{a,3}{1} = test_results{sort_val(a,2)}{1}([5 6]); % measured HRTF - whole stats
    
    for b=1:14 % test number
        test_results_sorted{a,2}{b+1} = test_results{sort_val(a,1)}{b+1};
        test_results_sorted{a,3}{b+1} = test_results{sort_val(a,2)}{b+1};
    end
    
end

% save sorted data
save('test_results_sorted.mat', 'test_results_sorted');