%% Script for processing results from subjective tests
% Author: Martin Novotny (novotm80@fel.cvut.cz)

close all; clear; clc;
load('test_results_sorted.mat'); % load file with sorted data
addpath('plotSpread'); % load folder with plotSpread function

set(0,'defaultAxesFontSize',18); % set default font size
visualization = 0; % trigger visualization (0 -> untriggered, NO image; 1 -> triggered, IMAGES)
ANOVA_trig = 0; % trigger for ANOVA results display (0 -> NO DISPLAY, 1 -> RESULTS DISPLAYED)
anova = {'off', 'on'};

%% Get information about respondents and visualize it
test_num = size(test_results_sorted{1,2},2)-1; % get number of passed tests
resp_num = size(test_results_sorted,1); % get number of respondents

% get demographic data
for rn = 1:resp_num % rn - respondent number
    age(rn) = test_results_sorted{rn,1}{3};
    sex(rn) = test_results_sorted{rn,1}{4};
    exp(rn) = test_results_sorted{rn,1}{5};
end

% sex grouping (0 - male; 1 - female)
sex_gr = zeros(size(sex));
sex_gr(sex=='F') = 1;

% group for ANOVA (no age)
group = [sex_gr; exp];

if visualization
    figure('units','normalized','outerposition',[0 0 1 1])
    
    % age histogram
    subplot(1,3,1)
    hs = histogram(age, 'FaceColor', 'b');
    
    hs.FaceAlpha = 0.85;
    hs.LineWidth = 1;

    grid on;
    grid minor;
    xlabel('Respondent age');
    ylabel('Number of respondents');
    
    title('Age structure');


    % gender comparison
    subplot(1,3,2)
    gender = {'Male', 'Female'};
    X = categorical(gender);
    X = reordercats(X, gender);

    br = bar(X, [count(sex, 'M'); count(sex, 'F');], 'FaceColor', 'b');
    br.FaceColor = 'flat';
    br.CData(2,:) = [1 0 0];
    br.Parent.YLim(2) = br.Parent.YLim(2)*1.05;
    
    % add value X/Y (X out of Y)
    str1 = sprintf('%0.0d/%0.0d', br.YData(1), resp_num);
    str2 = sprintf('%0.0d/%0.0d', br.YData(2), resp_num);

    text(1,br.YEndPoints(1),str1, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', 16);

    text(2,br.YEndPoints(2),str2, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', 16);

    grid on;
    grid minor;
    xlabel('Respondent gender');
    ylabel('Number of respondents');
    title('Gender structure');

    % VR experience
    subplot(1,3,3)
    VR_exp = {'1', '2', '3', '4', '5'};
    X = categorical(VR_exp);
    X = reordercats(X, VR_exp);
    
    exp_str = num2str(exp);

    br = bar(X, [count(exp_str, '1'); count(exp_str, '2'); count(exp_str, '3'); count(exp_str, '4'); count(exp_str, '5')], 'FaceColor', 'b');
    
    grid on;
    grid minor;
    xlabel('VR experience');
    ylabel('Number of respondents');
    title('VR experience structure');
    
    sgtitle('Respondents set structure', 'FontSize', 20);

    % save visualization
    cd test_visualization
        print('respondents_structure', '-dpng');
    cd ../
end

%% Get visualizations and statistical results for each test
test_type = {'Generic', 'Measured'}; % 
alpha = 0.05; % alpha for statistic testing (except for Lilliefors test -> alpha = 0.01)

for tn = 1:test_num %tn - test number
    
    % preallocate arrays (gen - generic HRTF, meas - measured HRTF)
    res_val_gen = zeros(resp_num, 3);
    res_val_meas = zeros(resp_num, 3);
    
    % fill array
    for rn = 1:resp_num 
        res_val_gen(rn,:) = test_results_sorted{rn,2}{tn+1};
        res_val_meas(rn,:) = test_results_sorted{rn,3}{tn+1};
    end
    
    if visualization
        % RESULTS VISUALIZATION
        % correct answers visualisation
        figure('units','normalized','outerposition',[0 0 1 1])
        subplot('Position', [0.1 0.15 0.25 0.725]);

        X = categorical(test_type);
        X = reordercats(X, test_type);

        br = bar(X, [sum(res_val_gen(:,1)); sum(res_val_meas(:,1))], 'FaceColor', 'b');
        br.FaceColor = 'flat';
        br.CData(2,:) = [1 0 0];
        br.Parent.YLim(2) = br.Parent.YLim(2)*1.05;
        
        % add value X/Y (X out of Y)
        str1 = sprintf('%0.0d/%0.0d', br.YData(1), resp_num);
        str2 = sprintf('%0.0d/%0.0d', br.YData(2), resp_num);

        text(1,br.YEndPoints(1),str1, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', 16);

        text(2,br.YEndPoints(2),str2, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', 16);

        grid on;
        grid minor;
        xlabel('Type of used HRTF');
        ylabel('Number of respondents');
        title('Correct answers');

        % reaction time visualisation
        subplot('Position', [0.4 0.15 0.25 0.725]);
        bp=boxplot([res_val_gen(:,2) res_val_meas(:,2)], test_type);
        set(bp,'LineWidth', 2);

        xlabel('Type of used HRTF');
        ylabel('Reaction time (s)');
        grid on;
        grid minor;

        title('Reaction time');


        % angle difference visualisation
        subplot('Position', [0.7 0.15 0.25 0.725]);

        ps = plotSpread([res_val_gen(:,3) res_val_meas(:,3)], 'xNames', test_type, 'showMM', 3, 'distributionColors', {'b', 'r'});
        
        % change median display style
        ps{2}.MarkerSize = 20;
        ps{2}.Marker = '+';
        ps{2}.Color = [1 0 1];
        ps{2}.LineWidth = 2.5;

        ps{3}.Children(2).MarkerSize = 15;
        ps{3}.Children(3).MarkerSize = 15;

        xlabel('Type of used HRTF');
        ylabel('Angular error ($^\circ$)');
        grid on;
        grid minor;

        title('Angular error');

        tit = sprintf('Test %0.2d results - visualization', tn);
        sgtitle(tit, 'FontSize', 22);
        
        % save results
        cd test_visualization
            fname = sprintf('test_%0.2d', tn);
            print(fname, '-dpng');
        cd ../
    end
    
    % STATISTICAL ANALYSIS
    
    % time per test
    % check normality
    [h_ll{1,tn}, p_ll{1,tn}, stats_ll{1,tn}] = lillietest(rmoutliers(res_val_gen(:,2)), 'alpha', 0.01);
    [h_ll{2,tn}, p_ll{2,tn}, stats_ll{2,tn}] = lillietest(rmoutliers(res_val_meas(:,2)), 'alpha', 0.01);
    
    % perform t-test and ANOVA
    [h{1,tn}, p{1,tn}, ~, stats{1,tn}] = ttest(res_val_gen(:,2), res_val_meas(:,2), 'alpha', alpha);
    [p_anov{1,tn}, tbl_anov{1,tn}, stats_anov{1,tn}] = anovan(mean([res_val_gen(:,2) res_val_meas(:,2)],2), group', 'model', 'interaction', 'varnames', {'Sex', 'Experience'}, 'display', anova{ANOVA_trig+1});
     
    % angle difference (SR and KW test)
    [p{2,tn}, h{2,tn}, stats{2,tn}] = signrank(res_val_gen(:,3), res_val_meas(:,3), 'alpha', alpha);
    [p_anov{2,tn}, tbl_anov{2,tn}, stats_anov{2,tn}] = kruskalwallis(mean([res_val_gen(:,3) res_val_meas(:,3)],2), exp, anova{ANOVA_trig+1});
    
          
end

%% Statistical analysis for whole test

% preallocate arrays (gen - generic HRTF, meas - measured HRTF)
res_val_gen_whole = zeros(resp_num, 2);
res_val_meas_whole = zeros(resp_num, 2);

% fill arrays with data
for rn = 1:resp_num
    % pass test data (conversion from cell to array)
    gen_help = test_results_sorted{rn,2}{1};
    meas_help = test_results_sorted{rn,3}{1};
    
    [res_val_gen_whole(rn,1), res_val_gen_whole(rn,2)] = gen_help{:};
    [res_val_meas_whole(rn,1), res_val_meas_whole(rn,2)] = meas_help{:};
end

% RESULTS VISUALIZATION

if visualization
    % time to pass the test
        figure('units','normalized','outerposition',[0 0 1 1])
        subplot(1,3,1)
        bp=boxplot([res_val_gen_whole(:,1) res_val_meas_whole(:,1)], test_type);
        set(bp,'LineWidth', 2);

        xlabel('Type of used HRTF');
        ylabel('Time to pass the test (s)');
        grid on;
        grid minor;

        tit = sprintf('Time to pass the whole test', tn);
        title(tit);

        % correct answers - GENERIC
        subplot(1,3,2)
        hs=histogram(res_val_gen_whole(:,2), 'FaceColor', 'b');

        hs.FaceAlpha = 0.85;
        hs.LineWidth = 1;

        xlabel('Number of correct answers');
        ylabel('Number of respondents');
        grid on;
        grid minor;

        tit = sprintf('Correct answers (whole test)', tn);
        title(tit);
        legend(test_type{1});
        
        % correct answers - MEASURED
        subplot(1,3,3)
        hs=histogram(res_val_meas_whole(:,2), 'FaceColor', 'r');
        hs.FaceAlpha = 0.85;
        hs.LineWidth = 1;

        xlabel('Number of correct answers');
        ylabel('Number of respondents');
        grid on;
        grid minor;

        tit = sprintf('Correct answers (whole test)', tn);
        title(tit);
        legend(test_type{2});

        cd test_visualization
            print('results_overall', '-dpng');
        cd ../
end
    
% STATISTICAL ANALYSIS
    % time per whole test
    % chcek normality
    [h_ll{3,1}, p_ll{3,1}, stats_ll{3,1}] = lillietest(res_val_gen_whole(:,1), 'alpha', 0.01);
    [h_ll{4,1}, p_ll{4,1}, stats_ll{4,1}] = lillietest(res_val_meas_whole(:,1), 'alpha', 0.01);
    
    % perform t-test and ANOVA
    [h{3,1}, p{3,1}, ~, stats{3,1}] = ttest(res_val_gen_whole(:,1), res_val_meas_whole(:,1), 'alpha', alpha);
    [p_anov{2,1}, tbl_anov{2,1}, stats_anov{2,1}] = anovan(mean([res_val_gen_whole(:,1) res_val_meas_whole(:,1)],2), group', 'model', 'interaction', 'varnames', {'Sex', 'Experience'}, 'display', anova{ANOVA_trig+1});
    
    % correct answers - SR test
    [p{4,1}, h{4,1}, stats{4,1}] = signrank(res_val_gen_whole(:,2), res_val_meas_whole(:,2), 'alpha', alpha);
