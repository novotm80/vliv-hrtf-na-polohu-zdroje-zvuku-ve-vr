%% Script for reading subjective test results
% Author: Martin Novotny (novotm80@fel.cvut.cz)

close all; clear; clc;
addpath('results_whole');

%% Get input data

% Load input data from selected folder
[file,path] = uigetfile('*.txt', 'Select test results', 'MultiSelect', 'on');

if isequal(file,0)
   errordlg('Nothing was selected');
else
    
idx_num = 1;
    for a = 1:size(file,2) % get number of files
        fname = [path file{a}];  % select data

        % read text and find endings (TEST TERMINATED)
        file_whole = fileread(fname);
        file_split = regexp(file_whole, '--------------------------TEST TERMINATED--------------------------');
        
        % split file into multiple pieces and save as separate file
        for idx = 1:size(file_split, 2)
            fname = sprintf('%03d.txt', idx_num);
            fid = fopen(fullfile('results', fname), 'w');

            if idx==1
                fwrite(fid, file_whole([1:file_split(idx)-1]));
            else
                fwrite(fid, file_whole([file_split(idx-1)+69:file_split(idx)-1])); %+69 -> length of TEST TERMINATED string
            end

            fclose(fid);
            idx_num = idx_num+1;
        end
    end
end

test_passed = idx_num-1;
save('test_passed.mat', 'test_passed');