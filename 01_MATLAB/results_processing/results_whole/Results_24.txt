
Vaclav_Stybr
-
Interpolation: Bilinear
Measured HRTF

TEST CASE: Test_A1_1 - 8deg angle - 1 source, 1 distractor, side by side
 - Correct
 - Time til answer: 5.018753 seconds

TEST CASE: Test_A4_4 - 15deg angle - 1 source, 1 distractor, side by side
 - Incorrect
 - Time til answer: 18.50256 seconds
Answer coord: (0.4, 1.2, 3.4)  |||  Source coord: (-0.5, 1.3, 3.5)  |||  Angle: 13.95502°

TEST CASE: Test_B2_6 - 5deg angle - 1 source, 2 distractors, triangle vertexes
 - Incorrect
 - Time til answer: 14.03866 seconds
Answer coord: (0.1, 1.2, 3.4)  |||  Source coord: (0.0, 1.4, 3.5)  |||  Angle: 3.977098°

TEST CASE: Test_B4_8 - 15deg angle - 1 source, 2 distractors, triangle vertexes
 - Correct
 - Time til answer: 25.61186 seconds

TEST CASE: Test_C1_9 - 8deg angle - 1 source, 3 distractors, triangle vertexes + center
 - Correct
 - Time til answer: 11.19505 seconds

TEST CASE: Test_C3_11 - 3deg angle - 1 source, 3 distractors, triangle vertexes + center
 - Correct
 - Time til answer: 35.11865 seconds

TEST CASE: Test_D3_15 - 3deg angle - 1 source, 3 distractors, square
 - Incorrect
 - Time til answer: 26.65376 seconds
Answer coord: (0.1, 1.3, 3.5)  |||  Source coord: (-0.1, 1.5, 3.5)  |||  Angle: 3.383387°

TEST CASE: Test_D4_16 - 15deg angle - 1 source, 3 distractors, square
 - Incorrect
 - Time til answer: 8.705994 seconds
Answer coord: (0.4, 0.9, 3.4)  |||  Source coord: (-0.5, 1.8, 3.5)  |||  Angle: 18.10926°

TEST CASE: Test_E1_17 - 8 deg - 1 source, 3 distractors, specified X movement, square
 - Incorrect
 - Time til answer: 18.72595 seconds
Answer coord: (-1.4, 0.8, 3.5)  |||  Source coord: (-1.5, 1.3, 3.5)  |||  Angle: 7.077564°

TEST CASE: Test_E4_20 - 1 source, 6 distractors, random X movement
 - Incorrect
 - Time til answer: 7.085205 seconds
Answer coord: (0.6, 0.7, 3.4)  |||  Source coord: (0.5, 1.6, 3.5)  |||  Angle: 12.81974°

TEST CASE: Test_F2_22 - 1 source, 6 distractors, random XY movement
 - Correct
 - Time til answer: 17.36935 seconds

TEST CASE: Test_H2_26 - 1 source + 6 distractors - The influence of source size 2
 - Correct
 - Time til answer: 23.16769 seconds

TEST CASE: Test_J2_30 - 3 sources, 7 distractors, moving distractor audio
 - Incorrect
 - Time til answer: 12.90628 seconds
Answer coord: (-0.1, 1.1, 2.6)  |||  Source coord: (-0.4, 1.7, 3.3)  |||  Angle: 6.506592°

TEST CASE: Test_J3_31 - 2 sources, 6 distractors, static distractor audio behind the player, with rain
 - Correct
 - Time til answer: 14.81628 seconds

Test duration: 4 min 8.87 seconds
--------------------------END OF TEST--------------------------
--------------------------TEST TERMINATED--------------------------
