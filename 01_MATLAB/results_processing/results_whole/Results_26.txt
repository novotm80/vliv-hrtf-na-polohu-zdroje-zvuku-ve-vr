
Vendula_Frankova
-
Interpolation: Bilinear
Measured HRTF

TEST CASE: Test_A1_1 - 8deg angle - 1 source, 1 distractor, side by side
 - Correct
 - Time til answer: 7.685848 seconds

TEST CASE: Test_A4_4 - 15deg angle - 1 source, 1 distractor, side by side
 - Correct
 - Time til answer: 13.46213 seconds

TEST CASE: Test_B2_6 - 5deg angle - 1 source, 2 distractors, triangle vertexes
 - Incorrect
 - Time til answer: 15.25923 seconds
Answer coord: (0.2, 1.1, 3.4)  |||  Source coord: (0.0, 1.4, 3.5)  |||  Angle: 5.189879°

TEST CASE: Test_B4_8 - 15deg angle - 1 source, 2 distractors, triangle vertexes
 - Correct
 - Time til answer: 9.773643 seconds

TEST CASE: Test_C1_9 - 8deg angle - 1 source, 3 distractors, triangle vertexes + center
 - Incorrect
 - Time til answer: 16.703 seconds
Answer coord: (0.4, 1.0, 3.4)  |||  Source coord: (0.0, 1.3, 3.5)  |||  Angle: 7.552994°

TEST CASE: Test_C3_11 - 3deg angle - 1 source, 3 distractors, triangle vertexes + center
 - Correct
 - Time til answer: 12.68196 seconds

TEST CASE: Test_D3_15 - 3deg angle - 1 source, 3 distractors, square
 - Correct
 - Time til answer: 11.46198 seconds

TEST CASE: Test_D4_16 - 15deg angle - 1 source, 3 distractors, square
 - Incorrect
 - Time til answer: 10.97379 seconds
Answer coord: (0.4, 0.9, 3.4)  |||  Source coord: (-0.5, 1.8, 3.5)  |||  Angle: 17.67541°

TEST CASE: Test_E1_17 - 8 deg - 1 source, 3 distractors, specified X movement, square
 - Incorrect
 - Time til answer: 12.39412 seconds
Answer coord: (-1.0, 0.8, 3.4)  |||  Source coord: (-1.0, 1.3, 3.5)  |||  Angle: 6.561154°

TEST CASE: Test_E4_20 - 1 source, 6 distractors, random X movement
 - Incorrect
 - Time til answer: 14.83859 seconds
Answer coord: (-0.5, 0.7, 3.4)  |||  Source coord: (-0.1, 1.6, 3.5)  |||  Angle: 13.49181°

TEST CASE: Test_F2_22 - 1 source, 6 distractors, random XY movement
 - Correct
 - Time til answer: 20.257 seconds

TEST CASE: Test_H2_26 - 1 source + 6 distractors - The influence of source size 2
 - Correct
 - Time til answer: 12.68314 seconds

TEST CASE: Test_J2_30 - 3 sources, 7 distractors, moving distractor audio
 - Incorrect
 - Time til answer: 12.35085 seconds
Answer coord: (-0.1, 1.0, 2.6)  |||  Source coord: (-0.4, 1.7, 3.3)  |||  Angle: 6.305215°

TEST CASE: Test_J3_31 - 2 sources, 6 distractors, static distractor audio behind the player, with rain
 - Incorrect
 - Time til answer: 10.75034 seconds
Answer coord: (0.6, 0.9, 3.0)  |||  Source coord: (1.3, 1.3, 3.5)  |||  Angle: 7.848004°

Test duration: 3 min 11.23 seconds
--------------------------END OF TEST--------------------------
--------------------------TEST TERMINATED--------------------------
