
Sasa_Fisman
-
Interpolation: Bilinear
Measured HRTF

TEST CASE: Test_A1_1 - 8deg angle - 1 source, 1 distractor, side by side
 - Correct
 - Time til answer: 4.042963 seconds

TEST CASE: Test_A4_4 - 15deg angle - 1 source, 1 distractor, side by side
 - Incorrect
 - Time til answer: 31.43075 seconds
Answer coord: (0.4, 1.2, 3.4)  |||  Source coord: (-0.5, 1.3, 3.5)  |||  Angle: 13.99243°

TEST CASE: Test_B2_6 - 5deg angle - 1 source, 2 distractors, triangle vertexes
 - Correct
 - Time til answer: 14.93811 seconds

TEST CASE: Test_B4_8 - 15deg angle - 1 source, 2 distractors, triangle vertexes
 - Incorrect
 - Time til answer: 23.9891 seconds
Answer coord: (0.0, 1.7, 3.4)  |||  Source coord: (-0.5, 1.0, 3.5)  |||  Angle: 13.40865°

TEST CASE: Test_C1_9 - 8deg angle - 1 source, 3 distractors, triangle vertexes + center
 - Incorrect
 - Time til answer: 3.465126 seconds
Answer coord: (0.4, 1.1, 3.4)  |||  Source coord: (0.0, 1.3, 3.5)  |||  Angle: 7.412856°

TEST CASE: Test_C3_11 - 3deg angle - 1 source, 3 distractors, triangle vertexes + center
 - Correct
 - Time til answer: 5.465187 seconds

TEST CASE: Test_D3_15 - 3deg angle - 1 source, 3 distractors, square
 - Incorrect
 - Time til answer: 11.97185 seconds
Answer coord: (0.1, 1.3, 3.5)  |||  Source coord: (-0.1, 1.5, 3.5)  |||  Angle: 3.430624°

TEST CASE: Test_D4_16 - 15deg angle - 1 source, 3 distractors, square
 - Correct
 - Time til answer: 31.98557 seconds

TEST CASE: Test_E1_17 - 8 deg - 1 source, 3 distractors, specified X movement, square
 - Correct
 - Time til answer: 21.6131 seconds

TEST CASE: Test_E4_20 - 1 source, 6 distractors, random X movement
 - Correct
 - Time til answer: 10.48442 seconds

TEST CASE: Test_F2_22 - 1 source, 6 distractors, random XY movement
 - Correct
 - Time til answer: 20.96951 seconds

TEST CASE: Test_H2_26 - 1 source + 6 distractors - The influence of source size 2
 - Correct
 - Time til answer: 10.12868 seconds

TEST CASE: Test_J2_30 - 3 sources, 7 distractors, moving distractor audio
 - Incorrect
 - Time til answer: 5.131042 seconds
Answer coord: (-0.1, 1.1, 2.6)  |||  Source coord: (-0.4, 1.7, 3.3)  |||  Angle: 6.021253°

TEST CASE: Test_J3_31 - 2 sources, 6 distractors, static distractor audio behind the player, with rain
 - Correct
 - Time til answer: 7.596054 seconds

Test duration: 3 min 33.16 seconds
--------------------------END OF TEST--------------------------
--------------------------TEST TERMINATED--------------------------
