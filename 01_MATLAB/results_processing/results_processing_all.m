%% Script for running all the processing at once
% Author: Martin Novotny (novotm80@fel.cvut.cz)

close all; clear; clc;

% add path to needed folders
addpath(genpath('scripts'));
addpath(genpath('resp_data'));

% set LATEX as default interpreter (for visulalisations)
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

%% Run scripts for analysis
results_split;
results_read;
results_processing;
